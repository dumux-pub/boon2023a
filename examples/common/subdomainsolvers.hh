// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief Darcy and Stokes solver classes to solve a stationary, single-phase problems.
 */
#ifndef DUMUX_MORTAR_STOKES_DARCY_SUBDOMAIN_SOLVERS_HH
#define DUMUX_MORTAR_STOKES_DARCY_SUBDOMAIN_SOLVERS_HH

#include <string>
#include <unordered_map>
#include <functional>

#include <dune/common/exceptions.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/id.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include "stress.hh"
#include "traceoperator.hh"
#include "mortarvariabletype.hh"

namespace Dumux {

/*!
 * \brief Interface for subdomain solvers.
 * \tparam SolutionVector Type used for solution vectors
 */
template<class SolutionVector, class Position>
class SubDomainSolver
{
public:
    virtual ~SubDomainSolver() {}

    //! Defines the states to be set
    enum class State { homogeneous, inhomogeneous };

    //! Solve the problem
    virtual void solve() = 0;

    //! Write current state to disk
    virtual void write(double time = 1.0) = 0;

    //! Set a state in the solver
    virtual void setState(State state) = 0;

    //! Sets the projection of the mortar variable of the interface with given id
    virtual void setMortarProjection(const Id& id, const SolutionVector& p) = 0;

    //! Returns the trace of the variable/derivative at the interface with given id
    virtual SolutionVector getTrace(const Id& id) const = 0;

    //! Set the id of this sub-domain
    void setId(const Id& id)
    { id_ = id; }

    //! Return the id of this sub-domain
    const Id& id() { return id_; };

    //! Compute the error in pressure
    virtual double computePressureError() const = 0;

    //! Compute the error in velocity
    virtual double computeVelocityError() const = 0;

    //! Retrieve the permeability field
    virtual const SolutionVector& permeability() const = 0;

    //! Set the interface permeability field to be used
    virtual void setInterfacePermeabilityField(const Id& id, const SolutionVector& sol) = 0;

    virtual void updateOutputFields() = 0;

    virtual void setTimeLoop(std::shared_ptr<Dumux::TimeLoop<double>> ptr) = 0;

    virtual void advanceTimeStep() = 0;

    virtual Position exactVelocity(const Position&) const
    { DUNE_THROW(Dune::NotImplemented, "Implementation does not overload exactVelocity()"); }

    virtual double exactPressure(const Position&) const
    { DUNE_THROW(Dune::NotImplemented, "Implementation does not overload exactPressure()"); }

private:
    Id id_;
};

/*!
 * \brief Darcy solver class to solve a stationary, single-phase problem.
 * \tparam TypeTag The type tag of the problem to be solved
 * \LinearSolver Optional argument to define the solver to be used for
 *               the solution of the linear system. We default to the
 *               direct linear solver UMFPack here, but support any other
 *               solver available in Dumux except for the AMGBackend, which
 *               needs additional template parameters and constructor arguments.
 */
template<class TypeTag,
         class MortarSolutionVector,
         class LinearSolver = Dumux::UMFPackBackend,
         bool useMarkers = false>
class DarcySolver
: public SubDomainSolver<
    MortarSolutionVector,
    typename GetPropType<TypeTag, Properties::GridGeometry>::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate
>
{
    using ThisType = DarcySolver<TypeTag, MortarSolutionVector, LinearSolver, useMarkers>;
    using GlobalPos = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;

    using ParentType = SubDomainSolver<MortarSolutionVector, GlobalPos>;
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using OutputModule = VtkOutputModule<GridVariables, SolutionVector>;


public:
    //! export some data types
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;

    using typename ParentType::State;
    using TraceOperator = Dumux::InterfaceTraceOperator<ThisType>;

    void init(const std::string& paramGroup, const Id& id, MortarVariableType mv)
    {
        mv_ = mv;
        ParentType::setId(id);

        // try to create a grid (from the given grid file or the input file)
        gridManager_.init(paramGroup);

        // we compute on the leaf grid view
        const auto& leafGridView = gridManager_.grid().leafGridView();

        // create the finite volume grid geometry
        fvGridGeometry_ = std::make_shared<GridGeometry>(leafGridView);
        fvGridGeometry_->update();

        // the problem (initial and boundary conditions)
        if constexpr (useMarkers)
        {
            auto params = std::make_shared<SpatialParams>(fvGridGeometry_, paramGroup, gridManager_.getGridData());
            problem_ = std::make_shared<Problem>(fvGridGeometry_, params, paramGroup, mv);
        }
        else
        {
            auto params = std::make_shared<SpatialParams>(fvGridGeometry_, paramGroup);
            problem_ = std::make_shared<Problem>(fvGridGeometry_, params, paramGroup, mv);
        }

        // resize and initialize the given solution vector
        x_ = std::make_shared<SolutionVector>();
        x_->resize(fvGridGeometry_->numDofs());
        problem_->applyInitialSolution(*x_);
        xOld_ = std::make_shared<SolutionVector>();
        xOldCopy_ = std::make_shared<SolutionVector>();
        *xOld_ = *x_;
        *xOldCopy_ = *xOld_;

        // the grid variables
        gridVariables_ = std::make_shared<GridVariables>(problem_, fvGridGeometry_);
        gridVariables_->init(*x_);

        // initialize the vtk output module
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
        vtkWriter_ = std::make_unique<OutputModule>(*gridVariables_, *x_, problem_->name());
        IOFields::initOutputModule(*vtkWriter_);
        problem_->addOutputFields(*vtkWriter_);
        vtkWriter_->addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables_));
        if (getParamFromGroup<bool>(paramGroup, "IO.WriteInitialSolution", false))
            vtkWriter_->write(0.0);

        assembler_ = std::make_shared<Assembler>(problem_, fvGridGeometry_, gridVariables_);
        auto linearSolver = std::make_shared<LinearSolver>();
        newtonSolver_ = std::make_unique<NewtonSolver>(assembler_, linearSolver);
    }

    void updateOutputFields() override
    {
        //problem_->addOutputFields(*vtkWriter_);
    }

    //! Solve the system
    void solve() override
    {
        if (timeLoop_)
            newtonSolver_->solve(*x_, *timeLoop_);
        else
            newtonSolver_->solve(*x_);
    }

    //! Write current state to disk
    void write(double t = 1.0) override
    {
        vtkWriter_->write(t);
    }

    //! Set a state in the solver
    void setState(State state) override
    {
        if (state == State::homogeneous)
        {
            *xOldCopy_ = *xOld_;
            *xOld_ = 0.0;
            problem_->setUseHomogeneousSetup(true);
        }
        else if (state == State::inhomogeneous)
        {
            *xOld_ = *xOldCopy_;
            problem_->setUseHomogeneousSetup(false);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Provided state flag not supported");
    }

    //! Sets the projection of the mortar variable of the interface with given id
    void setMortarProjection(const Id& id, const SolutionVector& p) override
    {
        problem_->setMortarProjection(id, p);
    }

    //! Returns the trace of the variable/derivative at the interface with given id
    MortarSolutionVector getTrace(const Id& id) const override
    {
        const auto& traceOperator = traceOperatorMap_.at(id.get());
        if (mv_ == MortarVariableType::solution)
            return traceOperator->recoverTraceFlux(*x_);
        else if (mv_ == MortarVariableType::derivative)
            return traceOperator->recoverTraceSolution(*x_);
        else
            DUNE_THROW(Dune::NotImplemented, "Unsupported mortar variable type");
    }

    //! Function to register a neighboring mortar segment
    template<class MortarSegment, class Glue>
    void registerMortarSegment(const MortarSegment& mortarSegment, const Glue& glue)
    {
        traceOperatorMap_[mortarSegment.id().get()] = std::make_unique<TraceOperator>(*this, mortarSegment.gridGeometry(), glue);
        problem_->registerMortarSegment(mortarSegment, *traceOperatorMap_.at(mortarSegment.id().get()));
    }

    //! Return reference to the grid geometry
    const GridGeometry& gridGeometry() const { return *fvGridGeometry_; }
    //! Return reference to the grid variables
    const GridVariables& gridVariables() const { return *gridVariables_; }
    //! Return reference to the problem
    Problem& problem() { return *problem_; }

    virtual double computePressureError() const override
    {
        double l2error = 0.0;
        const int order = getParam<int>("Error.DarcyPressureL2IntegrationOrder");
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(problem_->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& geo = scv.geometry();
                const auto& quad = Dune::QuadratureRules<double, GridGeometry::GridView::dimension>::rule(geo.type(), order);
                const double discretePressure = (*x_)[scv.dofIndex()];
                for (const auto& qp : quad)
                {
                    const auto error = problem_->exactPressure(geo.global(qp.position())) - discretePressure;
                    l2error += (error*error)*qp.weight()*geo.integrationElement(qp.position());
                }
            }
        }

        using std::sqrt;
        return sqrt(l2error);
    }

    virtual double computeVelocityError() const override
    {
        double l2error = 0.0;
        const int order = getParam<int>("Error.DarcyVelocityL2IntegrationOrder");
        for (const auto& element : elements(problem_->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(problem_->gridGeometry());
            auto elemVolVars = localView(gridVariables_->curGridVolVars());
            auto elemFluxVarsCache = localView(gridVariables_->gridFluxVarsCache());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, *x_);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            double elemFluxError = 0.0;
            for (const auto& scvf : scvfs(fvGeometry))
            {
                double discreteFlux;
                if (scvf.boundary() && problem_->boundaryTypes(element, scvf).hasOnlyNeumann())
                    discreteFlux = problem_->neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)
                                   /elemVolVars[scvf.insideScvIdx()].density()
                                   *scvf.area();
                else
                {
                    FluxVariables fluxVars;
                    fluxVars.init(*problem_, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    discreteFlux = fluxVars.advectiveFlux(0, [] (const auto& vv) { return vv.mobility(); });
                }

                // compute flux error
                discreteFlux /= scvf.area();
                double scvfError = 0.0;
                const auto& scvfGeometry = scvf.geometry();
                const auto& rule = Dune::QuadratureRules<double, GridGeometry::GridView::dimension-1>::rule(
                    scvfGeometry.type(), order
                );
                for (const auto& qp : rule)
                {
                    const auto& ipLocal = qp.position();
                    const auto& ipGlobal = scvfGeometry.global(ipLocal);
                    const auto error = discreteFlux - problem_->exactVelocity(ipGlobal)*scvf.unitOuterNormal();
                    scvfError += error*error*qp.weight()*scvfGeometry.integrationElement(ipLocal);
                }

                elemFluxError += scvfError/scvf.area();
            }

            elemFluxError *= element.geometry().volume();
            l2error += elemFluxError;
        }

        using std::sqrt;
        return sqrt(l2error);
    }

    virtual GlobalPos exactVelocity(const GlobalPos& pos) const override
    { return problem_->exactVelocity(pos); }

    virtual double exactPressure(const GlobalPos& pos) const override
    { return problem_->exactPressure(pos); }

    virtual const SolutionVector& permeability() const override
    { return problem_->permeability(); }

    virtual void setInterfacePermeabilityField(const Id& id, const SolutionVector& perm) override
    { problem_->setInterfacePermeabilityField(id, perm); }

    virtual void setTimeLoop(std::shared_ptr<Dumux::TimeLoop<double>> ptr) override
    {
        timeLoop_ = ptr;
        *assembler_ = Assembler(problem_, fvGridGeometry_, gridVariables_, timeLoop_, *xOld_);
    }

    virtual void advanceTimeStep() override
    {
        *xOld_ = *x_;
        *xOldCopy_ = *xOld_;
        gridVariables_->advanceTimeStep();
    }


private:
    MortarVariableType mv_;

    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager_;
    std::shared_ptr<GridGeometry> fvGridGeometry_;
    std::shared_ptr<Problem> problem_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::shared_ptr<Assembler> assembler_;

    std::unique_ptr<NewtonSolver> newtonSolver_;
    std::unique_ptr<OutputModule> vtkWriter_;

    std::shared_ptr<SolutionVector> x_;
    std::shared_ptr<SolutionVector> xOld_;
    std::shared_ptr<SolutionVector> xOldCopy_;
    std::unordered_map<std::size_t, std::unique_ptr<TraceOperator>> traceOperatorMap_;
    std::shared_ptr<Dumux::TimeLoop<double>> timeLoop_ = nullptr;
};

/*!
 * \brief Stokes solver class to solve a stationary, single-phase problem.
 * \tparam TypeTag The type tag of the problem to be solved
 * \LinearSolver Optional argument to define the solver to be used for
 *               the solution of the linear system. We default to the
 *               direct linear solver UMFPack here, but support any other
 *               solver available in Dumux except for the AMGBackend, which
 *               needs additional template parameters and constructor arguments.
 */
template<class TypeTag, class MortarSolutionVector, class LinearSolver = Dumux::UMFPackBackend>
class StokesSolver
: public SubDomainSolver<
    MortarSolutionVector,
    typename GetPropType<TypeTag, Properties::GridGeometry>::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate
>
{
    using ThisType = StokesSolver<TypeTag, MortarSolutionVector, LinearSolver>;
    using GlobalPos = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
    using ParentType = SubDomainSolver<MortarSolutionVector, GlobalPos>;

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using OutputModule = StaggeredVtkOutputModule<GridVariables, SolutionVector>;

    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;

public:
    //! export some data types
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using typename ParentType::State;
    using TraceOperator = Dumux::InterfaceTraceOperator<ThisType>;

    void init(const std::string& paramGroup, const Id& id, MortarVariableType mv)
    {
        mv_ = mv;
        ParentType::setId(id);

        using GlobalPosition = typename GridGeometry::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
        const auto cut0 = getParam<GlobalPosition>("Stokes.Grid.CutLowerLeft", GlobalPosition(1e15));
        const auto cut1 = getParam<GlobalPosition>("Stokes.Grid.CutUpperRight", GlobalPosition(-1e15));

        auto elementSelector = [&](const auto& element)
        {
            const auto pos = element.geometry().center();
            const auto diff0 = pos - cut0;
            const auto diff1 = cut1 - pos;
            if (std::all_of(diff0.begin(), diff0.end(), [] (double x) { return x > 1e-6; })
                && std::all_of(diff1.begin(), diff1.end(), [] (double x) { return x > 1e-6; }))
                return false;
            return true;
        };

        // try to create a grid (from the given grid file or the input file)
        hostGridManager_.init(paramGroup);
        gridManager_.init(hostGridManager_.grid(), elementSelector, paramGroup);

        // we compute on the leaf grid view
        const auto& leafGridView = gridManager_.grid().leafGridView();

        // create the finite volume grid geometry
        fvGridGeometry_ = std::make_shared<GridGeometry>(leafGridView);
        fvGridGeometry_->update();

        // the problem (initial and boundary conditions)
        problem_ = std::make_shared<Problem>(fvGridGeometry_, paramGroup, mv);

        // resize and initialize the given solution vector
        x_ = std::make_shared<SolutionVector>();
        (*x_)[GridGeometry::cellCenterIdx()].resize(fvGridGeometry_->numCellCenterDofs());
        (*x_)[GridGeometry::faceIdx()].resize(fvGridGeometry_->numFaceDofs());
        problem_->applyInitialSolution(*x_);
        xOld_ = std::make_shared<SolutionVector>();
        xOldCopy_ = std::make_shared<SolutionVector>();
        *xOld_ = *x_;
        *xOldCopy_ = *xOld_;

        // the grid variables
        gridVariables_ = std::make_shared<GridVariables>(problem_, fvGridGeometry_);
        gridVariables_->init(*x_);

        // initialize the vtk output module
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        vtkWriter_ = std::make_unique<OutputModule>(*gridVariables_, *x_, problem_->name());
        IOFields::initOutputModule(*vtkWriter_);

        assembler_ = std::make_shared<Assembler>(problem_, fvGridGeometry_, gridVariables_);
        auto linearSolver = std::make_shared<LinearSolver>();
        newtonSolver_ = std::make_unique<NewtonSolver>(assembler_, linearSolver);

        sigmaX_.resize(fvGridGeometry_->gridView().size(0), GlobalPos(0.0));
        sigmaY_.resize(fvGridGeometry_->gridView().size(0), GlobalPos(0.0));
        vtkWriter_->addField(sigmaX_, "sigmaX");
        vtkWriter_->addField(sigmaY_, "sigmaY");
        problem_->addOutputFields(*vtkWriter_);
        if (getParamFromGroup<bool>(paramGroup, "IO.WriteInitialSolution", false))
            vtkWriter_->write(0.0);
    }

    void updateOutputFields() override
    {
        // using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        // using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        // using Stress = NavierStokesStressRecovery<double, ModelTraits, PrimaryVariables>;
        // const auto [sigmax, sigmay] = Stress::assembleStresses(*problem_, *gridVariables_, *x_);
        // std::copy(sigmax.begin(), sigmax.end(), sigmaX_.begin());
        // std::copy(sigmay.begin(), sigmay.end(), sigmaY_.begin());
    }

    //! Solve the system
    void solve() override
    {
        if (timeLoop_)
            newtonSolver_->solve(*x_, *timeLoop_);
        else
            newtonSolver_->solve(*x_);
    }

    //! Write current state to disk
    void write(double t = 1.0) override
    {
        updateOutputFields();
        vtkWriter_->write(t);
    }

    //! Set a state in the solver
    void setState(State state) override
    {
        if (state == State::homogeneous)
        {
            *xOldCopy_ = *xOld_;
            *xOld_ = 0.0;
            problem_->setUseHomogeneousSetup(true);
        }
        else if (state == State::inhomogeneous)
        {
            *xOld_ = *xOldCopy_;
            problem_->setUseHomogeneousSetup(false);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Provided state flag not supported");
    }

    //! Sets the projection of the mortar variable of the interface with given id
    void setMortarProjection(const Id& id, const MortarSolutionVector& p) override
    {
        problem_->setMortarProjection(id, p);
    }

    //! Returns the trace of the variable/derivative at the interface with given id
    MortarSolutionVector getTrace(const Id& id) const override
    {
        const auto& traceOperator = traceOperatorMap_.at(id.get());
        if (mv_ == MortarVariableType::solution)
            return traceOperator->recoverTraceFlux(*x_);
        else if (mv_ == MortarVariableType::derivative)
            return traceOperator->recoverTraceSolution(*x_);
        else
            DUNE_THROW(Dune::NotImplemented, "Unsupported mortar variable type");
    }


    //! Function to register a neighboring mortar segment
    template<class MortarSegment, class Glue>
    void registerMortarSegment(const MortarSegment& mortarSegment, const Glue& glue)
    {
        traceOperatorMap_[mortarSegment.id().get()] = std::make_unique<TraceOperator>(*this, mortarSegment.gridGeometry(), glue);
        problem_->registerMortarSegment(mortarSegment, *traceOperatorMap_.at(mortarSegment.id().get()));
    }

    //! Return reference to the grid geometry
    const GridGeometry& gridGeometry() const { return *fvGridGeometry_; }
    //! Return reference to the grid variables
    const GridVariables& gridVariables() const { return *gridVariables_; }
    //! Return reference to the problem
    Problem& problem() { return *problem_; }

    double computePressureError() const override
    {
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using Indices = typename ModelTraits::Indices;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using L2Error = NavierStokesTestL2Error<double, ModelTraits, PrimaryVariables>;
        const auto l2error = L2Error::calculateL2Error(*problem_, *x_);
        return std::sqrt(l2error[Indices::pressureIdx]);
    }

    double computeVelocityError() const override
    {
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using Error = NavierStokesTestError<double, ModelTraits, PrimaryVariables>;
        auto result = Error::calculateError(*problem_, gridVariables(), *x_);
        return result.velocityNorm();
    }

    virtual GlobalPos exactVelocity(const GlobalPos& pos) const override
    { return problem_->exactVelocity(pos); }

    virtual double exactPressure(const GlobalPos& pos) const override
    { return problem_->exactPressure(pos); }

    virtual const MortarSolutionVector& permeability() const override
    { return problem_->permeability(); }

    virtual void setInterfacePermeabilityField(const Id& id, const MortarSolutionVector& perm)
    { problem_->setInterfacePermeabilityField(id, perm); }

    virtual void setTimeLoop(std::shared_ptr<Dumux::TimeLoop<double>> ptr) override
    {
        timeLoop_ = ptr;
        *assembler_ = Assembler(problem_, fvGridGeometry_, gridVariables_, timeLoop_, *xOld_);
    }

    virtual void advanceTimeStep() override
    {
        *xOld_ = *x_;
        *xOldCopy_ = *xOld_;
        gridVariables_->advanceTimeStep();
    }

private:
    MortarVariableType mv_;
    GridManager<typename GetProp<TypeTag, Properties::Grid>::HostGrid> hostGridManager_;
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager_;
    std::shared_ptr<GridGeometry> fvGridGeometry_;
    std::shared_ptr<Problem> problem_;
    std::shared_ptr<GridVariables> gridVariables_;
    std::shared_ptr<Assembler> assembler_;

    std::unique_ptr<NewtonSolver> newtonSolver_;
    std::unique_ptr<OutputModule> vtkWriter_;

    std::shared_ptr<SolutionVector> x_;
    std::shared_ptr<SolutionVector> xOld_;
    std::shared_ptr<SolutionVector> xOldCopy_;
    std::unordered_map<std::size_t, std::unique_ptr<TraceOperator>> traceOperatorMap_;
    std::shared_ptr<Dumux::TimeLoop<double>> timeLoop_ = nullptr;

    std::vector<GlobalPos> sigmaX_;
    std::vector<GlobalPos> sigmaY_;
};

} // end namespace Dumux

#endif // DUMUX_MORTAR_STOKES_DARCY_SUBDOMAIN_SOLVERS_HH
