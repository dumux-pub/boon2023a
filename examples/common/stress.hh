// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \copydoc Dumux::StressReconstruction
 */
#ifndef DUMUX_BOON2020A_COMMON_STRESS_HH
#define DUMUX_BOON2020A_COMMON_STRESS_HH

#include <vector>
#include <cmath>

#include <dune/common/float_cmp.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dumux/common/integrate.hh>
#include <dumux/common/typetraits/problem.hh>
#include <dumux/discretization/extrusion.hh>
#include <dumux/freeflow/navierstokes/staggered/velocitygradients.hh>

namespace Dumux {

//! Recover stress tensors in the domain for a given solution
template<class Scalar, class ModelTraits, class PrimaryVariables>
class NavierStokesStressRecovery
{
    using Indices = typename ModelTraits::Indices;

public:
    template<class Problem, class GridVariables, class SolutionVector>
    static auto assembleStresses(const Problem& problem,
                                 const GridVariables& gridVariables,
                                 const SolutionVector& curSol)
    {
        using GridGeometry = typename ProblemTraits<Problem>::GridGeometry;
        using Vector = Dune::FieldVector<double, GridGeometry::GridView::dimension>;
        return assembleStresses_<Vector>(
            problem,
            problem.gridGeometry(),
            gridVariables.curGridVolVars(),
            gridVariables.curGridFaceVars(),
            curSol
        );
    }

private:
    template<class Vector,
             class Problem,
             class GridGeometry,
             class GridVolVars,
             class GridFaceVars,
             class SolutionVector>
    static auto assembleStresses_(const Problem& problem,
                                  const GridGeometry& gridGeometry,
                                  const GridVolVars& gridVolVars,
                                  const GridFaceVars& gridFaceVars,
                                  const SolutionVector& curSol)
    {
        static_assert(int(GridGeometry::GridView::dimension) == 2);
        static_assert(int(GridGeometry::GridView::dimensionworld) == 2);

        using BoundaryTypes = typename ProblemTraits<Problem>::BoundaryTypes;
        using Gradients = StaggeredVelocityGradients<Scalar,
                                                     GridGeometry,
                                                     BoundaryTypes,
                                                     typename ModelTraits::Indices>;

        std::vector<Scalar> coefficientsDvxDvx(gridGeometry.gridView().size(0), 0.0);
        std::vector<Scalar> coefficientsDvyDvy(gridGeometry.gridView().size(0), 0.0);
        std::vector<Scalar> coefficientsDvxDvy(gridGeometry.gridView().size(GridGeometry::GridView::dimension), 0.0);
        std::vector<Scalar> coefficientsDvyDvx(gridGeometry.gridView().size(GridGeometry::GridView::dimension), 0.0);
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            const auto eIdx = gridGeometry.elementMapper().index(element);
            auto fvGeometry = localView(gridGeometry);
            auto elemFaceVars = localView(gridFaceVars);
            fvGeometry.bind(element);
            elemFaceVars.bind(element, fvGeometry, curSol);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto dir = scvf.directionIndex();
                const auto grad = Gradients::velocityGradII(scvf, elemFaceVars[scvf])*scvf.directionSign();
                if (dir == 0 && scvf.unitOuterNormal()[dir] > 0)
                    coefficientsDvxDvx[gridGeometry.elementMapper().index(element)] = -1.0*grad; // it seems that velocity grad returns with flipped sign?
                else if (dir == 1 && scvf.unitOuterNormal()[dir] > 0)
                    coefficientsDvyDvy[gridGeometry.elementMapper().index(element)] = -1.0*grad;
            }

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const std::size_t numSubFaces = scvf.pairData().size();
                for (std::size_t localSubFaceIdx = 0; localSubFaceIdx < numSubFaces; ++localSubFaceIdx)
                {
                    const auto& lateralFace = fvGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localLateralFaceIdx);

                    std::optional<BoundaryTypes> currentFaceBoundaryTypes;
                    std::optional<BoundaryTypes> lateralFaceBoundaryTypes;
                    if (scvf.boundary()) currentFaceBoundaryTypes.emplace(problem.boundaryTypes(element, scvf));
                    if (lateralFace.boundary()) lateralFaceBoundaryTypes.emplace(problem.boundaryTypes(element, lateralFace));

                    const auto dir = scvf.directionIndex();
                    const auto lateralDir = lateralFace.directionIndex();
                    const auto grad = Gradients::velocityGradIJ(
                        problem,
                        element,
                        fvGeometry,
                        scvf,
                        elemFaceVars[scvf],
                        currentFaceBoundaryTypes,
                        lateralFaceBoundaryTypes,
                        localSubFaceIdx
                    );

                    const auto vertexIndex = gridGeometry.vertexMapper().subIndex(
                        element,
                        getOverlappingCornerIndex_(element, scvf, lateralFace),
                        GridGeometry::GridView::dimension
                    );
                    if (dir == 0 && lateralDir == 1)
                        coefficientsDvxDvy[vertexIndex] = grad;
                    else if (dir == 1 && lateralDir == 0)
                        coefficientsDvyDvx[vertexIndex] = grad;
                    else
                        DUNE_THROW(Dune::InvalidStateException, "Unexpected dir/lateraldir combination");
                }
            }
        }

        using Q0Basis = Dune::Functions::LagrangeBasis<typename GridGeometry::GridView, 0>;
        using Q1Basis = Dune::Functions::LagrangeBasis<typename GridGeometry::GridView, /*order*/1>;
        Q0Basis q0Basis{gridGeometry.gridView()};
        Q1Basis q1basis{gridGeometry.gridView()};

        auto fdxdx = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q0Basis, coefficientsDvxDvx);
        auto fdydy = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q0Basis, coefficientsDvyDvy);
        auto fdxdy = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q1basis, coefficientsDvxDvy);
        auto fdydx = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q1basis, coefficientsDvyDvx);

        double viscosity = getParam<double>("0.Component.Viscosity");
        std::vector<Vector> sigma_x(gridGeometry.gridView().size(0));
        std::vector<Vector> sigma_y(gridGeometry.gridView().size(0));
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            const auto& geo = element.geometry();
            const auto dvxdx = [&] () { auto f = localFunction(fdxdx); f.bind(element); return f(geo.local(geo.center())); } ();
            const auto dvydy = [&] () { auto f = localFunction(fdydy); f.bind(element); return f(geo.local(geo.center())); } ();
            const auto dvxdy = [&] () { auto f = localFunction(fdxdy); f.bind(element); return f(geo.local(geo.center())); } ();
            const auto dvydx = [&] () { auto f = localFunction(fdydx); f.bind(element); return f(geo.local(geo.center())); } ();

            const auto eIdx = gridGeometry.elementMapper().index(element);
            auto fvGeometry = localView(gridGeometry); fvGeometry.bindElement(element);
            auto elemVolVars = localView(gridVolVars); elemVolVars.bindElement(element, fvGeometry, curSol);
            const auto p = elemVolVars[*scvs(fvGeometry).begin()].pressure();
            sigma_x[eIdx][0] = 2.0*viscosity*dvxdx - p;
            sigma_x[eIdx][1] = viscosity*(dvxdy + dvydx) - p;

            sigma_y[eIdx][0] = 2.0*viscosity*dvydy - p;
            sigma_y[eIdx][1] = viscosity*(dvxdy + dvydx) - p;
        }

        return std::make_pair(sigma_x, sigma_y);
    }

    template<typename Element, typename SCVF>
    static auto getOverlappingCornerIndex_(const Element& e,
                                           const SCVF& scvf1,
                                           const SCVF& scvf2)
    { return getLocalCornerIndex_(e, getOverlappingCorner_(scvf1, scvf2)); }

    template<typename SCVF>
    static auto getOverlappingCorner_(const SCVF& scvf1, const SCVF& scvf2)
    {
        const auto geo1 = scvf1.geometry();
        const auto geo2 = scvf2.geometry();
        for (unsigned i = 0; i < geo1.corners(); ++i)
            for (unsigned j = 0; j < geo2.corners(); ++j)
                if (Dune::FloatCmp::eq(geo1.corner(i), geo2.corner(j), 1e-6))
                    return geo1.corner(i);
        DUNE_THROW(Dune::InvalidStateException, "Could not find overlapping scvf corner");
    }

    template<typename Element, typename GlobalPosition>
    static auto getLocalCornerIndex_(const Element& e, const GlobalPosition& pos)
    {
        const auto& geo = e.geometry();
        for (unsigned i = 0; i < e.subEntities(Element::Geometry::mydimension); ++i)
            if (Dune::FloatCmp::eq(geo.corner(i), pos, 1e-6))
                return i;
        DUNE_THROW(Dune::InvalidStateException, "Could not find element corner");
    }
};

} // end namespace Dumux

#endif
