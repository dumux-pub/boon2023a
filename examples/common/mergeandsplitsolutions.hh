// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_STOKES_DARCY_MERGE_AND_SPLIT_INTERFACE_SOLUTION_HH
#define DUMUX_MORTAR_STOKES_DARCY_MERGE_AND_SPLIT_INTERFACE_SOLUTION_HH

#include <cassert>
#include <algorithm>

#include <dune/common/exceptions.hh>

namespace Dumux {
namespace Impl {

    template<class MortarSegment>
    std::vector<std::size_t> computeDofOffsets(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments,
                                               std::size_t& numDofs)
    {
        std::vector<std::size_t> segmentNumDofs(mortarSegments.size());
        for (auto segment : mortarSegments)
            segmentNumDofs[segment->id().get()] = segment->gridGeometry().numDofs();

        std::vector<std::size_t> offsets;
        offsets.reserve(mortarSegments.size());
        offsets.push_back(0);
        for (std::size_t i = 1; i < mortarSegments.size(); ++i)
            offsets.push_back( offsets[i-1] + segmentNumDofs[i-1] );

        numDofs = offsets.back() + segmentNumDofs.back();
        return offsets;
    }

    template<class MortarSegment>
    std::vector<std::size_t> computeDofOffsets(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments)
    {
        std::size_t numDofs;
        return computeDofOffsets(mortarSegments, numDofs);
    }

} // end namespace Impl

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 * \note This assumes the segment ids() to go from 0 to n and that
 *       all ids in between are taken by the objects contained in mortarSegments.
 */
template<class MortarSegment, class SolutionVector>
void extractMortarSolutions(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments,
                            const SolutionVector& solution,
                            std::vector<SolutionVector>& solutionVectors)
{
    const auto offsets = Impl::computeDofOffsets(mortarSegments);

    solutionVectors.resize(mortarSegments.size());
    for (auto segment : mortarSegments)
    {
        auto& x = solutionVectors[segment->id().get()];
        x.resize(segment->gridGeometry().numDofs());

        std::size_t idx = offsets[segment->id().get()];
        for (std::size_t i = 0; i < segment->gridGeometry().numDofs(); ++i)
        {
            assert(solution.size() >= idx+1);
            x[i] = solution[idx++];
        }
    }
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSegment, class SolutionVector>
std::vector<SolutionVector>
extractMortarSolutions(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments,
                       const SolutionVector& solution)
{
    std::vector<SolutionVector> solutionVectors;
    extractMortarSolutions(mortarSegments, solution, solutionVectors);
    return solutionVectors;
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 * \note This assumes the segment ids() to go from 0 to n and that
 *       all ids in between are taken by the objects contained in mortarSegments.
 */
template<class MortarSegment, class SolutionVector>
void mergeMortarSolutions(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments,
                          const std::vector<SolutionVector>& solutionVectors,
                          SolutionVector& solution)
{
    if (solutionVectors.size() != mortarSegments.size())
        DUNE_THROW(Dune::InvalidStateException,
                   "Number of provided solution vectors does not match number of segments");

    std::size_t numDofs;
    const auto offsets = Impl::computeDofOffsets(mortarSegments, numDofs);

    solution.resize(numDofs);
    for (auto segment : mortarSegments)
    {
        std::size_t idx = offsets[segment->id().get()];
        for (std::size_t i = 0; i < segment->gridGeometry().numDofs(); ++i)
            solution[idx++] = solutionVectors[segment->id().get()][i];
    }
}

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSegment, class SolutionVector>
SolutionVector mergeMortarSolutions(const std::vector<std::shared_ptr<MortarSegment>>& mortarSegments,
                                    const std::vector<SolutionVector>& solutionVectors)
{
    SolutionVector solution;
    mergeMortarSolutions(mortarSegments, solutionVectors, solution);
    return solution;
}

} // end namespace Dumux

#endif // DUMUX_MORTAR_STOKES_DARCY_EXTRACT_INTERFACE_SOLUTION_HH
