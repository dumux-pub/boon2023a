// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_SUBDOMAIN_INTERFACE_HH
#define DUMUX_MORTAR_SUBDOMAIN_INTERFACE_HH

#include <utility>

#include <dune/common/exceptions.hh>
#include "projectorinterface.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MS, class SD, class S>
class Interface
{
public:
    //! Export the underlying types
    using MortarSegment = MS;
    using SubDomain = SD;
    using SolutionVector = S;
    using Projector = MortarProjectorInterface<SolutionVector>;
    using NormalVector = typename MortarSegment::GridGeometry::GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;

    enum class ProjectionDirection { toSubDomain, toMortar };

    /*!
     * \brief TODO doc me.
     */
    Interface(std::shared_ptr<MortarSegment> mortarSegment,
              std::shared_ptr<SubDomain> subDomain,
              NormalVector normal)
    : mortarSegment_(mortarSegment)
    , subDomain_(subDomain)
    , projectorToSubDomain_(nullptr)
    , projectorToMortarSegment_(nullptr)
    , normal_(std::move(normal))
    {}

    //! Return the neighboring mortar segment
    std::shared_ptr<MortarSegment> mortarSegmentPointer() const { return mortarSegment_; }
    MortarSegment& mortarSegment() const { return *mortarSegment_; }
    //! Return the neighboring sub-domain
    std::shared_ptr<SubDomain> subDomainPointer() const { return subDomain_; }
    SubDomain& subDomain() const { return *subDomain_; }

    //! set the projector to the sub domain
    template<class ProjectorImpl>
    void setProjectorToSubDomain(std::shared_ptr<ProjectorImpl> p)
    {
        static_assert(std::is_convertible_v<ProjectorImpl*, Projector*>,
                      "Provided projector does not implement the projector interface");
        projectorToSubDomain_ = p;
    }

    //! set the projector to the mortar segment
    template<class ProjectorImpl>
    void setProjectorToMortarSegment(std::shared_ptr<ProjectorImpl> p)
    {
        static_assert(std::is_convertible_v<ProjectorImpl*, Projector*>,
                      "Provided projector does not implement the projector interface");
        projectorToMortarSegment_ = p;
    }

    //! Return the projector into the sub-domain
    const Projector& getProjector(ProjectionDirection direction) const
    {
        if (direction == ProjectionDirection::toSubDomain)
        {
            if (!projectorToSubDomain_)
                DUNE_THROW(Dune::InvalidStateException, "Projector to subdomain not set");
            return *projectorToSubDomain_;
        }
        else
        {
            if (!projectorToMortarSegment_)
                DUNE_THROW(Dune::InvalidStateException, "Projector to mortar segment not set");
            return *projectorToMortarSegment_;
        }
    }

    const NormalVector& normal() const
    { return normal_; }

private:
    std::shared_ptr<MortarSegment> mortarSegment_;
    std::shared_ptr<SubDomain> subDomain_;

    std::shared_ptr<Projector> projectorToSubDomain_;
    std::shared_ptr<Projector> projectorToMortarSegment_;
    NormalVector normal_;
};

} // end namespace Dumux

#endif
