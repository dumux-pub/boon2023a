// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_HELPERS_HH
#define DUMUX_MORTAR_HELPERS_HH

#include <dumux/geometry/geometryintersection.hh>

namespace Dumux {

template<typename FVGeometry, typename Geometry>
auto getIntersectingNormalVector(const FVGeometry& fvGeometry, const Geometry& geo)
{
    using Policy = Dumux::IntersectionPolicy::SegmentPolicy<double, Geometry::coorddimension>;
    typename Policy::Intersection is;
    for (const auto& scvf : scvfs(fvGeometry))
        if (Dumux::GeometryIntersection<
                std::decay_t<decltype(scvf.geometry())>,
                Geometry
            >::template intersection<Policy>(scvf.geometry(), geo, is))
            return scvf.unitOuterNormal();
    DUNE_THROW(Dune::InvalidStateException, "Could not determine intersection normal");
}

} // end namespace Dumux

#endif
