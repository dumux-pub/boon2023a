// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_PROJECTOR_HH
#define DUMUX_MORTAR_PROJECTOR_HH

#include <dumux/discretization/projection/projector.hh>
#include "projectorinterface.hh"

namespace Dumux {

template<class SolutionVector>
class StandardProjector : public MortarProjectorInterface<SolutionVector>
{
    using Scalar = typename SolutionVector::field_type;
    using Projector = Dumux::Projector<Scalar>;

public:
    //! Export the type used for projection matrices
    using ProjectionMatrix = typename Projector::Matrix;

    /*!
     * \brief TODO doc me.
     */
    StandardProjector(ProjectionMatrix&& massMatrix, ProjectionMatrix&& projMatrix)
    : projector_(std::move(massMatrix), std::move(projMatrix))
    {}

    //! project a solution vector
    SolutionVector project(const SolutionVector& x) const override
    { return projector_.project(x); }

private:
    Projector projector_;
};

template<class SolutionVector>
class TransposedProjector : public MortarProjectorInterface<SolutionVector>
{
    using Scalar = typename SolutionVector::field_type;
    using Projector = Dumux::Projector<Scalar>;

public:
    //! Export the type used for projection matrices
    using ProjectionMatrix = typename Projector::Matrix;

    /*!
     * \brief TODO doc me.
     */
    TransposedProjector(ProjectionMatrix&& projMatrix)
    : B_(std::move(projMatrix))
    {}

    /*!
     * \brief TODO doc me.
     */
    SolutionVector project(const SolutionVector& x) const
    {
        SolutionVector result;
        result.resize(B_.M());

        B_.mtv(x, result);
        return result;
    }

private:
    ProjectionMatrix B_;
};

} // end namespace Dumux

#endif
