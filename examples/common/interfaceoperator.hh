// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH
#define DUMUX_MORTAR_ONEP_INTERFACE_OPERATOR_HH

#include <dune/istl/operators.hh>
#include <dumux/common/id.hh>

#include "projector.hh"
#include "mortarvariabletype.hh"
#include "mergeandsplitsolutions.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSegment,
         class SubDomainSolver,
         class InterfaceMapper,
         class SolutionVector>
class MortarInterfaceOperator
: public Dune::LinearOperator< SolutionVector, SolutionVector >
{
    using FieldType =  typename SolutionVector::field_type;

    using MortarSegmentPtr = std::shared_ptr<MortarSegment>;
    using SolverPtr = std::shared_ptr<SubDomainSolver>;
    using Interface = typename InterfaceMapper::Interface;

public:
    explicit MortarInterfaceOperator(std::vector<MortarSegmentPtr> mortarSegments,
                                     std::vector<SolverPtr> solvers,
                                     std::shared_ptr<InterfaceMapper> interfaceMapper,
                                     MortarVariableType mv)
    : mortarSegments_(mortarSegments)
    , solverPtrs_(solvers)
    , interfaceMapper_(interfaceMapper)
    , variableType_(mv)
    {
        segmentSolutions_.resize(mortarSegments.size());
        if ( variableType_ != MortarVariableType::solution
             && variableType_ != MortarVariableType::derivative )
            DUNE_THROW(Dune::InvalidStateException, "Unkown mortar variable type");
    }

    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply(const SolutionVector& x, SolutionVector& r) const
    {
        // extract interface sub-solutions
        extractMortarSolutions(mortarSegments_, x, segmentSolutions_);

        // project mortar variable into sub-domains
        for (auto segment : mortarSegments_)
        {
            const auto& segmentSol = segmentSolutions_[segment->id().get()];

            const auto& posInterface = interfaceMapper_->getPositiveInterface(*segment);
            const auto& negInterface = interfaceMapper_->getNegativeInterface(*segment);

            const auto& posProjector = posInterface.getProjector(Interface::ProjectionDirection::toSubDomain);
            const auto& negProjector = negInterface.getProjector(Interface::ProjectionDirection::toSubDomain);

            const auto posProjection = posProjector.project(segmentSol);
            auto negProjection = negProjector.project(segmentSol);
            if (variableType_ == MortarVariableType::derivative)
                negProjection *= -1.0;

            posInterface.subDomain().setMortarProjection(segment->id(), posProjection);
            negInterface.subDomain().setMortarProjection(segment->id(), negProjection);
        }

        // solve the sub-domains
        for (auto solver : solverPtrs_)
            solver->solve();

        // compute the jump in mortar variable across all interfaces
        std::vector<SolutionVector> jumps(mortarSegments_.size());
        for (auto segment : mortarSegments_)
        {
            const auto& posInterface = interfaceMapper_->getPositiveInterface(*segment);
            const auto& negInterface = interfaceMapper_->getNegativeInterface(*segment);

            const auto& posSubDomain = posInterface.subDomain();
            const auto& negSubDomain = negInterface.subDomain();

            const auto posTrace = posSubDomain.getTrace(segment->id());
            auto negTrace = negSubDomain.getTrace(segment->id());

            if (variableType_ == MortarVariableType::derivative)
                negTrace *= -1.0;

            // project trace into mortar space
            const auto posProjection = posInterface.getProjector(Interface::ProjectionDirection::toMortar).project(posTrace);
            const auto negProjection = negInterface.getProjector(Interface::ProjectionDirection::toMortar).project(negTrace);

            if (posProjection.size() != negProjection.size())
                DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have the same size");
            if (posProjection.size() != segment->gridGeometry().numDofs())
                DUNE_THROW(Dune::InvalidStateException, "Trace projections do not have size of mortar segment space");

            jumps[segment->id().get()] = posProjection;
            jumps[segment->id().get()] += negProjection;
        }

        // merge jumps into residual vector
        mergeMortarSolutions(mortarSegments_, jumps, r);
    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd(FieldType alpha, const SolutionVector& x, SolutionVector& y) const
    {
        SolutionVector yTmp;

        apply(x, yTmp);
        yTmp *= alpha;

        y += yTmp;
    }

    //! Category of the solver (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    { return Dune::SolverCategory::sequential; }

private:
    std::vector<MortarSegmentPtr> mortarSegments_;
    std::vector<SolverPtr> solverPtrs_;
    std::shared_ptr<InterfaceMapper> interfaceMapper_;

    MortarVariableType variableType_;
    mutable std::vector<SolutionVector> segmentSolutions_;
};

} // end namespace Dumux

#endif
