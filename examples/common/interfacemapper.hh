// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_INTERFACE_MAPPER_HH
#define DUMUX_MORTAR_INTERFACE_MAPPER_HH

#include <unordered_map>
#include <dune/common/exceptions.hh>

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief Maps to instances of MortarSegment the
 *        interfaces to the neighboring subdomains.
 */
template<class MortarSegment, class IF>
class InterfaceMapper
{
public:
    // export interface type
    using Interface = IF;

    //! Adds an interface for a mortar segment
    void addInterface(std::shared_ptr<MortarSegment> mortarSegment,
                      std::shared_ptr<Interface> interface)
    {
        if (!posInterfaceMap_.count(mortarSegment->id().get()))
            posInterfaceMap_[mortarSegment->id().get()] = interface;
        else if (!negInterfaceMap_.count(mortarSegment->id().get()))
            negInterfaceMap_[mortarSegment->id().get()] = interface;
        else
            DUNE_THROW(Dune::InvalidStateException,
                       "Both interfaces already defined for segment with id " << mortarSegment->id().get());
    }

    //! Return the positive interface for a given segment
    const Interface& getPositiveInterface(const MortarSegment& mortarSegment) const
    { return *posInterfaceMap_.at(mortarSegment.id().get()); }

    //! Return the positive interface for a given segment
    const Interface& getNegativeInterface(const MortarSegment& mortarSegment) const
    { return *negInterfaceMap_.at(mortarSegment.id().get()); }

private:
    std::unordered_map<std::size_t, std::shared_ptr<Interface>> posInterfaceMap_;
    std::unordered_map<std::size_t, std::shared_ptr<Interface>> negInterfaceMap_;
};

} // end namespace Dumux

#endif
