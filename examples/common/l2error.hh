// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \copydoc Dumux::NavierStokesTestL2Error
 */
#ifndef DUMUX_TEST_L2_ERROR_HH
#define DUMUX_TEST_L2_ERROR_HH

#include <vector>
#include <cmath>

#include <dune/common/float_cmp.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dumux/common/integrate.hh>
#include <dumux/common/typetraits/problem.hh>
#include <dumux/discretization/extrusion.hh>
#include <dumux/freeflow/navierstokes/staggered/velocitygradients.hh>

namespace Dumux {

/*!
 * \ingroup NavierStokesTests
 * \brief Routines to calculate the discrete L2 error
 */
template<class Scalar, class ModelTraits, class PrimaryVariables>
class NavierStokesTestL2Error
{
    using Indices = typename ModelTraits::Indices;

public:

    /*!
      * \brief Calculates the L2 error between the analytical solution and the numerical approximation.
      *
      * \param problem The object specifying the problem which ought to be simulated
      * \param curSol Vector containing the current solution
      */
    template<class Problem, class SolutionVector>
    static auto calculateL2Error(const Problem& problem, const SolutionVector& curSol)
    {
        using GridGeometry = std::decay_t<decltype(problem.gridGeometry())>;
        static_assert(int(GridGeometry::GridView::dimension) == 2);

        PrimaryVariables l2NormSquared(0.0);
        const int porder = getParam<int>("Error.StokesPressureL2IntegrationOrder");
        const int vorder = getParam<int>("Error.StokesVelocityL2IntegrationOrder");
        for (const auto& element : elements(problem.gridGeometry().gridView()))
        {
            auto fvGeometry = localView(problem.gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& geo = scv.geometry();
                const auto discretePressure = curSol[GridGeometry::cellCenterIdx()][scv.dofIndex()][Indices::pressureIdx - ModelTraits::dim()];
                const auto& quad = Dune::QuadratureRules<Scalar, GridGeometry::GridView::dimension>::rule(geo.type(), porder);
                for (const auto& qp : quad)
                {
                    const auto error = problem.exactPressure(geo.global(qp.position())) - discretePressure;
                    l2NormSquared[Indices::pressureIdx] += (error*error)*qp.weight()*geo.integrationElement(qp.position());
                }
            }

            PrimaryVariables elemVelocityError(0.0);
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const int dofIdxFace = scvf.dofIndex();
                const int dirIdx = scvf.directionIndex();
                if (dirIdx != 0 && dirIdx != 1)
                    DUNE_THROW(Dune::InvalidStateException, "Unexpected direction index");

                const auto discreteVelocity = curSol[GridGeometry::faceIdx()][dofIdxFace][0];

                // std::vector<GlobalPosition> faceScvCorners;
                // const int otherDirIdx = dirIdx == 0 ? 1 : 0;
                // GlobalPosition diffVec(0.0); diffVec[otherDirIdx] = 0.5*getDiscLengthIn_(otherDirIdx, geo);
                // faceScvCorners.push_back(scv.center()); faceScvCorners.back() -= diffVec;
                // faceScvCorners.push_back(scvf.center()); faceScvCorners.back() -= diffVec;
                // faceScvCorners.push_back(scv.center()); faceScvCorners.back() += diffVec;
                // faceScvCorners.push_back(scvf.center()); faceScvCorners.back() += diffVec;
                // Dune::MultiLinearGeometry<double, 2, 2> faceGeo{
                //     Dune::GeometryTypes::cube(2),
                //     faceScvCorners
                // };
                const auto& faceGeo = scvf.geometry();
                const auto& faceQuad = Dune::QuadratureRules<Scalar, GridGeometry::GridView::dimension-1>::rule(faceGeo.type(), vorder);
                double faceError = 0.0;
                for (const auto& qp : faceQuad)
                {
                    const auto velError = problem.exactVelocity(faceGeo.global(qp.position()))[Indices::velocity(dirIdx)] - discreteVelocity;
                    faceError += (velError*velError)*qp.weight()*faceGeo.integrationElement(qp.position());
                }
                elemVelocityError[Indices::velocity(dirIdx)] += faceError/faceGeo.volume();
            }

            l2NormSquared[Indices::velocity(0)] += elemVelocityError[Indices::velocity(0)]*element.geometry().volume();
            l2NormSquared[Indices::velocity(1)] += elemVelocityError[Indices::velocity(1)]*element.geometry().volume();
        }

        return l2NormSquared;
    }

private:
    template<typename Geometry>
    static auto getDiscLengthIn_(int direction, const Geometry& geo)
    {
        std::vector<typename Geometry::ctype> positions;
        for (int i = 0; i < geo.corners(); ++i)
            positions.push_back(geo.corner(i)[direction]);
        std::sort(positions.begin(), positions.end());

        std::vector<typename Geometry::ctype> deltas;
        std::adjacent_difference(positions.begin(), positions.end(), std::back_inserter(deltas));
        return *std::max_element(deltas.begin()+1, deltas.end());
    }
};


template<class Scalar, class ModelTraits, class PrimaryVariables>
class NavierStokesTestError
{
    using Indices = typename ModelTraits::Indices;

public:
    struct Result
    {
        PrimaryVariables l2ErrorSquared;
        std::optional<Scalar> errorSquaredDvxDx;
        std::optional<Scalar> errorSquaredDvyDy;
        std::optional<Scalar> errorSquaredDvxDy;
        std::optional<Scalar> errorSquaredDvyDx;

        Scalar velocityNorm() const
        {
            if (getParam<bool>("Error.UseStokesVelocityL2Norm"))
                return std::sqrt(
                    l2ErrorSquared[Indices::velocity(0)]
                    + l2ErrorSquared[Indices::velocity(1)]
                );
            return std::sqrt(
                l2ErrorSquared[Indices::velocity(0)]
                + l2ErrorSquared[Indices::velocity(1)]
                + errorSquaredDvxDx.value()
                + errorSquaredDvyDy.value()
                + errorSquaredDvxDy.value()
                + errorSquaredDvyDx.value()
            );
        }
    };
     /*!
      * \brief Calculates the L2 error between the analytical solution and the numerical approximation.
      *
      * \param problem The object specifying the problem which ought to be simulated
      * \param curSol Vector containing the current solution
      */
    template<class Problem, class GridVariables, class SolutionVector>
    static auto calculateError(const Problem& problem,
                               const GridVariables& gridVariables,
                               const SolutionVector& curSol)
    {
        Result result;
        result.l2ErrorSquared = NavierStokesTestL2Error<Scalar, ModelTraits, PrimaryVariables>::calculateL2Error(
            problem, curSol
        );

        if (!getParam<bool>("Error.UseStokesVelocityL2Norm"))
        {
            const auto l2norm = result.l2ErrorSquared;
            result = calculateH1Error_(
                problem,
                problem.gridGeometry(),
                gridVariables.curGridVolVars(),
                gridVariables.curGridFaceVars(),
                curSol
            );
            result.l2ErrorSquared = l2norm;
        }

        return result;
    }

private:
    template<class Problem,
             class GridGeometry,
             class GridVolVars,
             class GridFaceVars,
             class SolutionVector>
    static auto calculateH1Error_(const Problem& problem,
                                  const GridGeometry& gridGeometry,
                                  const GridVolVars& gridVolVars,
                                  const GridFaceVars& gridFaceVars,
                                  const SolutionVector& curSol)
    {
        static_assert(int(GridGeometry::GridView::dimension) == 2);
        static_assert(int(GridGeometry::GridView::dimensionworld) == 2);

        using BoundaryTypes = typename ProblemTraits<Problem>::BoundaryTypes;
        using Gradients = StaggeredVelocityGradients<Scalar,
                                                     GridGeometry,
                                                     BoundaryTypes,
                                                     typename ModelTraits::Indices>;

        std::vector<Scalar> coefficientsDvxDvx(gridGeometry.gridView().size(0), 0.0);
        std::vector<Scalar> coefficientsDvyDvy(gridGeometry.gridView().size(0), 0.0);
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            auto elemFaceVars = localView(gridFaceVars);
            fvGeometry.bind(element);
            elemFaceVars.bind(element, fvGeometry, curSol);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto dir = scvf.directionIndex();
                const auto grad = Gradients::velocityGradII(scvf, elemFaceVars[scvf])*scvf.directionSign();
                if (dir == 0 && scvf.unitOuterNormal()[dir] > 0)
                    coefficientsDvxDvx[gridGeometry.elementMapper().index(element)] = -1.0*grad; // it seems that velocity grad returns with flipped sign?
                else if (dir == 1 && scvf.unitOuterNormal()[dir] > 0)
                    coefficientsDvyDvy[gridGeometry.elementMapper().index(element)] = -1.0*grad;
            }
        }

        std::vector<Scalar> coefficientsDvxDvy(gridGeometry.gridView().size(GridGeometry::GridView::dimension), 0.0);
        std::vector<Scalar> coefficientsDvyDvx(gridGeometry.gridView().size(GridGeometry::GridView::dimension), 0.0);
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            const auto eIdx = gridGeometry.elementMapper().index(element);
            auto fvGeometry = localView(gridGeometry);
            auto elemFaceVars = localView(gridFaceVars);
            fvGeometry.bind(element);
            elemFaceVars.bind(element, fvGeometry, curSol);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const std::size_t numSubFaces = scvf.pairData().size();
                for (std::size_t localSubFaceIdx = 0; localSubFaceIdx < numSubFaces; ++localSubFaceIdx)
                {
                    const auto& lateralFace = fvGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localLateralFaceIdx);

                    std::optional<BoundaryTypes> currentFaceBoundaryTypes;
                    std::optional<BoundaryTypes> lateralFaceBoundaryTypes;
                    if (scvf.boundary()) currentFaceBoundaryTypes.emplace(problem.boundaryTypes(element, scvf));
                    if (lateralFace.boundary()) lateralFaceBoundaryTypes.emplace(problem.boundaryTypes(element, lateralFace));

                    const auto dir = scvf.directionIndex();
                    const auto lateralDir = lateralFace.directionIndex();
                    const auto grad = Gradients::velocityGradIJ(
                        problem,
                        element,
                        fvGeometry,
                        scvf,
                        elemFaceVars[scvf],
                        currentFaceBoundaryTypes,
                        lateralFaceBoundaryTypes,
                        localSubFaceIdx
                    );

                    const auto vertexIndex = gridGeometry.vertexMapper().subIndex(
                        element,
                        getOverlappingCornerIndex_(element, scvf, lateralFace),
                        GridGeometry::GridView::dimension
                    );
                    if (dir == 0 && lateralDir == 1)
                        coefficientsDvxDvy[vertexIndex] = grad;
                    else if (dir == 1 && lateralDir == 0)
                        coefficientsDvyDvx[vertexIndex] = grad;
                    else
                        DUNE_THROW(Dune::InvalidStateException, "Unexpected dir/lateraldir combination");
                }
            }
        }

        using Q0Basis = Dune::Functions::LagrangeBasis<typename GridGeometry::GridView, 0>;
        using Basis = Dune::Functions::LagrangeBasis<typename GridGeometry::GridView, /*order*/1>;
        Q0Basis q0Basis{gridGeometry.gridView()};
        Basis basis{gridGeometry.gridView()};

        auto fdxdx = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q0Basis, coefficientsDvxDvx);
        auto dxdxExact = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocityGradient(pos, 0, 0);
        }, gridGeometry.gridView());

        auto fdydy = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(q0Basis, coefficientsDvyDvy);
        auto dydyExact = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocityGradient(pos, 1, 1);
        }, gridGeometry.gridView());

        auto fdxdy = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(basis, coefficientsDvxDvy);
        auto dxdyExact = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocityGradient(pos, 0, 1);
        }, gridGeometry.gridView());

        auto fdydx = Dune::Functions::makeDiscreteGlobalBasisFunction<Scalar>(basis, coefficientsDvyDvx);
        auto dydxExact = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocityGradient(pos, 1, 0);
        }, gridGeometry.gridView());

        const int order = getParam<int>("Error.StokesVelocityH1IntegrationOrder");
        const auto errorDvxDx = integrateL2Error(gridGeometry.gridView(), fdxdx, dxdxExact, order);
        const auto errorDvyDy = integrateL2Error(gridGeometry.gridView(), fdydy, dydyExact, order);
        const auto errorDvxDy = integrateL2Error(gridGeometry.gridView(), fdxdy, dxdyExact, order);
        const auto errorDvyDx = integrateL2Error(gridGeometry.gridView(), fdydx, dydxExact, order);

        auto vx = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocity(pos)[0];
        }, gridGeometry.gridView());
        auto vy = Dune::Functions::makeAnalyticGridViewFunction([&] (const auto& pos) {
            return problem.exactVelocity(pos)[1];
        }, gridGeometry.gridView());

        // write vtk of grads
        Dune::VTKWriter<typename GridGeometry::GridView> writer{gridGeometry.gridView()};
        writer.addCellData(
            fdxdx,
            Dune::VTK::FieldInfo("dvxdx", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.addCellData(
            dxdxExact,
            Dune::VTK::FieldInfo("dvxdvxExact", Dune::VTK::FieldInfo::Type::scalar, 1)
        );

        writer.addCellData(
            vx,
            Dune::VTK::FieldInfo("vx", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.addCellData(
            vy,
            Dune::VTK::FieldInfo("vy", Dune::VTK::FieldInfo::Type::scalar, 1)
        );

        writer.addCellData(
            fdydy,
            Dune::VTK::FieldInfo("dvydy", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.addCellData(
            dydyExact,
            Dune::VTK::FieldInfo("dvydvyExact", Dune::VTK::FieldInfo::Type::scalar, 1)
        );

        writer.addVertexData(
            fdxdy,
            Dune::VTK::FieldInfo("dvxdvy", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.addVertexData(
            dxdyExact,
            Dune::VTK::FieldInfo("dxdyExact", Dune::VTK::FieldInfo::Type::scalar, 1)
        );

        writer.addVertexData(
            fdydx,
            Dune::VTK::FieldInfo("dvydvx", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.addVertexData(
            dydxExact,
            Dune::VTK::FieldInfo("dydxExact", Dune::VTK::FieldInfo::Type::scalar, 1)
        );
        writer.write("derivs");

        Result result;
        result.errorSquaredDvxDx = errorDvxDx*errorDvxDx;
        result.errorSquaredDvyDy = errorDvyDy*errorDvyDy;
        result.errorSquaredDvxDy = errorDvxDy*errorDvxDy;
        result.errorSquaredDvyDx = errorDvyDx*errorDvyDx;
        return result;
    }

    template<typename Element, typename SCVF>
    static auto getOverlappingCornerIndex_(const Element& e,
                                           const SCVF& scvf1,
                                           const SCVF& scvf2)
    { return getLocalCornerIndex_(e, getOverlappingCorner_(scvf1, scvf2)); }

    template<typename SCVF>
    static auto getOverlappingCorner_(const SCVF& scvf1, const SCVF& scvf2)
    {
        const auto geo1 = scvf1.geometry();
        const auto geo2 = scvf2.geometry();
        for (unsigned i = 0; i < geo1.corners(); ++i)
            for (unsigned j = 0; j < geo2.corners(); ++j)
                if (Dune::FloatCmp::eq(geo1.corner(i), geo2.corner(j), 1e-6))
                    return geo1.corner(i);
        DUNE_THROW(Dune::InvalidStateException, "Could not find overlapping scvf corner");
    }

    template<typename Element, typename GlobalPosition>
    static auto getLocalCornerIndex_(const Element& e, const GlobalPosition& pos)
    {
        const auto& geo = e.geometry();
        for (unsigned i = 0; i < e.subEntities(Element::Geometry::mydimension); ++i)
            if (Dune::FloatCmp::eq(geo.corner(i), pos, 1e-6))
                return i;
        DUNE_THROW(Dune::InvalidStateException, "Could not find element corner");
    }
};

} // end namespace Dumux

#endif
