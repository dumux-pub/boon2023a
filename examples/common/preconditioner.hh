// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_INTERFACE_PRECONDITIONER_HH
#define DUMUX_MORTAR_INTERFACE_PRECONDITIONER_HH

#include <dune/istl/preconditioner.hh>

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class MortarSolutionVector>
class MortarPreconditioner
: public Dune::Preconditioner<MortarSolutionVector, MortarSolutionVector>
{
public:
    /*!
     * \ingroup TODO doc me.
     * \brief TODO doc me.
     */
    explicit MortarPreconditioner() {}

    /*!
     * \brief Prepare the preconditioner.
     */
    virtual void pre (MortarSolutionVector& x, MortarSolutionVector& b) override
    {}

    /*!
     * \brief Apply one step of the preconditioner to the system A(v)=d.
     */
    virtual void apply (MortarSolutionVector& r, const MortarSolutionVector& x) override
    {
        r = x;
    }

    /*!
     * \brief Clean up.
     */
    virtual void post (MortarSolutionVector& x) override
    {}

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    { return Dune::SolverCategory::sequential; }
};

} // end namespace Dumux

#endif
