// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_PROJECTOR_FACTORY_HH
#define DUMUX_MORTAR_PROJECTOR_FACTORY_HH

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dumux/discretization/method.hh>

#include "projectorinterface.hh"
#include "projector.hh"
#include "mortarvariabletype.hh"

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
template<class SolutionVector>
class ProjectorFactory
{
    using ProjectorInterface = MortarProjectorInterface<SolutionVector>;

    // helper function to extract the discretization method
    template<class GridGeometry>
    constexpr DiscretizationMethod getDiscMethod(const GridGeometry& gg) const
    { return GridGeometry::discMethod; }

public:
    //! Constructor
    ProjectorFactory(MortarVariableType mv)
    : mv_(mv)
    {}

    //! Makes the projectors between a subdomain and a mortar segment
    template<class SubDomain, class MortarSegment, class Glue>
    void makeProjectors(const SubDomain& subDomain,
                        const MortarSegment& mortarSegment,
                        const Glue& glue,
                        std::shared_ptr<ProjectorInterface>& toSubDomain,
                        std::shared_ptr<ProjectorInterface>& toMortarSegment)
    {
        using namespace Dune::Functions;
        if ( mv_ == MortarVariableType::derivative
             && getDiscMethod(subDomain.gridGeometry()) == DiscretizationMethod::box )
        {
            // use P1 lagrange basis for sub-domain
            using Basis = LagrangeBasis<typename SubDomain::GridGeometry::GridView, 1>;
            Basis subDomainBasis(subDomain.gridGeometry().gridView());
            makeProjectors_(subDomainBasis, mortarSegment.gridGeometry().feBasis(), glue, toSubDomain, toMortarSegment);
        }
        else
        {
            // use P0 lagrange basis for sub-domain
            using Basis = LagrangeBasis<typename SubDomain::GridGeometry::GridView, 0>;
            Basis subDomainBasis(subDomain.gridGeometry().gridView());
            makeProjectors_(subDomainBasis, mortarSegment.gridGeometry().feBasis(), glue, toSubDomain, toMortarSegment);
        }
    }

private:
    //! Makes the projectors from the function space bases
    template<class MortarBasis, class SubDomainBasis, class Glue>
    static void makeProjectors_(const SubDomainBasis& subDomainBasis,
                                const MortarBasis& mortarBasis,
                                const Glue& glue,
                                std::shared_ptr<ProjectorInterface>& toSubDomain,
                                std::shared_ptr<ProjectorInterface>& toMortarSegment)
    {
        auto matrixPair = makeProjectionMatricesPair(subDomainBasis, mortarBasis, glue);
        toSubDomain = std::make_shared<StandardProjector<SolutionVector>>( std::move(matrixPair.second.first),
                                                                           std::move(matrixPair.second.second) );
        toMortarSegment = std::make_shared<StandardProjector<SolutionVector>>( std::move(matrixPair.first.first),
                                                                               std::move(matrixPair.first.second) );
    }

    MortarVariableType mv_;
};

} // end namespace Dumux

#endif
