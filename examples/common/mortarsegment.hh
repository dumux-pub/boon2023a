// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_MORTAR_SEGMENT_HH
#define DUMUX_MORTAR_SEGMENT_HH

#include <dumux/common/id.hh>

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief Class that defines a part of the mortar domain, described
 *        by an individual grid.
 */
template<class GG>
class MortarSegment
{
public:
    //! Export the grid geometry
    using GridGeometry = GG;

    /*!
     * \brief TODO doc me.
     */
    MortarSegment(std::shared_ptr<GridGeometry> gg, Id id)
    : id_(id)
    , gridGeometry_(gg)
    {}

    //! The identifier of this segment
    Id id() const { return id_; }

    //! Return the grid geometry of this segment
    GridGeometry& gridGeometry() { return *gridGeometry_; }
    const GridGeometry& gridGeometry() const { return *gridGeometry_; }

private:
    Id id_;                                      //!< Unique identifier of this segment
    std::shared_ptr<GridGeometry> gridGeometry_; //!< Grid geometry decribing this segment
};

} // end namespace Dumux

#endif
