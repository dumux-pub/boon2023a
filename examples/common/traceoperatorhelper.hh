// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
#ifndef DUMUX_ONEP_MORTAR_INTERFACE_TRACE_OPERATOR_HELPER_HH
#define DUMUX_ONEP_MORTAR_INTERFACE_TRACE_OPERATOR_HELPER_HH

#include <unordered_map>
#include <dumux/common/indextraits.hh>

namespace Dumux {

/*!
 * \ingroup TODO doc me.
 * \brief TODO doc me.
 */
class InterfaceTraceOperatorHelper
{
    template<class IndexType>
    using IndexVector = std::vector< IndexType >;

public:
    //! Stores the element indices and scvfs of those elements that lie on an interface
    template<class IndexType>
    using ElementScvfIndexMap = std::unordered_map< IndexType, IndexVector<IndexType> >;

    /*!
     * \brief TODO doc me.
     */
    template<class SubDomainGridGeometry, class MortarSegmentGridGeometry, class GlueType>
    static void findCoupledElements(const SubDomainGridGeometry& sdGridGeometry,
                                    const MortarSegmentGridGeometry& mortarSegmentGridGeometry,
                                    const GlueType& glue,
                                    std::vector<std::size_t>& coupledSdElements,
                                    std::vector<std::size_t>& couledInterfaceElements)
    {
        // store which elements are coupled
        coupledSdElements.clear();
        couledInterfaceElements.clear();
        coupledSdElements.reserve(glue.size());
        couledInterfaceElements.reserve(glue.size());
        for (const auto& is : intersections(glue))
        {
            coupledSdElements.push_back( sdGridGeometry.elementMapper().index(is.domainEntity(0)) );
            for (unsigned int nIdx = 0; nIdx < is.numTargetNeighbors(); ++nIdx)
                couledInterfaceElements.push_back( mortarSegmentGridGeometry.elementMapper().index(is.targetEntity(nIdx)) );
        }
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SDGridGeometry, class MortarSegmentGridGeometry>
    static ElementScvfIndexMap< typename IndexTraits<typename SDGridGeometry::GridView>::GridIndex >
    findCoupledScvfs(const SDGridGeometry& sdGridGeometry,
                     const MortarSegmentGridGeometry& mortarSegmentGridGeometry,
                     const std::vector<std::size_t>& coupledSdElements,
                     const std::vector<std::size_t>& couledInterfaceElements)
    {
        using ctype = typename SDGridGeometry::GridView::ctype;
        static constexpr ctype eps = 1.5e-7;

        using IndexType = typename IndexTraits<typename SDGridGeometry::GridView>::GridIndex;
        ElementScvfIndexMap<IndexType> map;
        for (auto eIdxMortar : couledInterfaceElements)
        {
            const auto& mortarElement = mortarSegmentGridGeometry.element(eIdxMortar);
            const auto& mortarElementGeometry = mortarElement.geometry();
            const auto& mortarElementCenter = mortarElementGeometry.center();
            const auto& mortarElementCorner = mortarElementGeometry.corner(1);
            auto mortarElementEdge = mortarElementCorner - mortarElementGeometry.corner(0);
            mortarElementEdge /= mortarElementEdge.two_norm();

            // TODO: USE GEOMETRY INTERSECTION HERE
            for (auto eIdxSubDomain : coupledSdElements)
            {
                const auto sdElement = sdGridGeometry.element(eIdxSubDomain);
                auto fvGeometry = localView(sdGridGeometry);
                fvGeometry.bind(sdElement);

                using std::abs;
                for (const auto& scvf : scvfs(fvGeometry))
                {
                    // check if face and mortar element are parallel
                    if ( abs(scvf.unitOuterNormal()*mortarElementEdge) > eps )
                        continue;

                    // check if they are in the same plane
                    auto d = scvf.center() - mortarElementCenter;
                    if (d.two_norm() < eps)
                        d = scvf.center() - mortarElementCorner;
                    d /= d.two_norm();

                    // check if the overlap
                    if ( abs(scvf.unitOuterNormal()*d) < eps )
                        map[eIdxSubDomain].push_back( scvf.index() );
                }
            }
        }

        // remove duplicates
        for (auto& entry : map)
        {
            auto& v = entry.second;
            std::sort(v.begin(), v.end());
            v.erase( std::unique(v.begin(), v.end()), v.end() );
        }

        return map;
    }

    /*!
     * \brief TODO doc me.
     */
    template<class SDGridGeometry, class MortarSegmentGridGeometry, class Glue>
    static ElementScvfIndexMap< typename IndexTraits<typename SDGridGeometry::GridView>::GridIndex >
    findCoupledScvfs(const SDGridGeometry& sdGridGeometry,
                     const MortarSegmentGridGeometry& mortarSegmentGridGeometry,
                     const Glue& glue)
    {
        std::vector<std::size_t> coupledSdElements;
        std::vector<std::size_t> couledInterfaceElements;
        findCoupledElements(sdGridGeometry, mortarSegmentGridGeometry, glue,
                            coupledSdElements, couledInterfaceElements);

        return findCoupledScvfs(sdGridGeometry, mortarSegmentGridGeometry,
                                coupledSdElements, couledInterfaceElements);
    }
};

} // end namespace Dumux

#endif
