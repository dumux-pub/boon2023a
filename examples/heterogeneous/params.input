# some parameters common to all problem
[Problem]
EnableGravity = false
EnableInertiaTerms = false
PressureDifference = 1

[FreeFlow]
EnableUnsymmetrizedVelocityGradient = false
EnableUnsymmetrizedVelocityGradientForBeaversJoseph = false

[Grid]
DomainMarkers = true

[Darcy]
SpatialParams.AlphaBJ = 1.0
SubDomainLength = 1.5

[SubDomain0]
SolverType = Darcy
Grid.File = mesh_0.msh
SpatialParams.PermeabilityMapFile = permeability_maps_0.txt
Problem.Name = darcy_0

[SubDomain1]
SolverType = Darcy
Grid.File = mesh_1.msh
SpatialParams.PermeabilityMapFile = permeability_maps_1.txt
Problem.Name = darcy_1

[SubDomain2]
SolverType = Darcy
Grid.File = mesh_2.msh
SpatialParams.PermeabilityMapFile = permeability_maps_2.txt
Problem.Name = darcy_2

[SubDomain3]
SolverType = Darcy
Grid.File = mesh_3.msh
SpatialParams.PermeabilityMapFile = permeability_maps_3.txt
Problem.Name = darcy_3

[SubDomain4]
SolverType = Darcy
Grid.File = mesh_4.msh
SpatialParams.PermeabilityMapFile = permeability_maps_4.txt
Problem.Name = darcy_4

[SubDomain5]
SolverType = Darcy
Grid.File = mesh_5.msh
SpatialParams.PermeabilityMapFile = permeability_maps_5.txt
Problem.Name = darcy_5

[SubDomain6]
SolverType = Darcy
Grid.File = mesh_6.msh
SpatialParams.PermeabilityMapFile = permeability_maps_6.txt
Problem.Name = darcy_6

[SubDomain7]
SolverType = Darcy
Grid.File = mesh_7.msh
SpatialParams.PermeabilityMapFile = permeability_maps_7.txt
Problem.Name = darcy_7

[SubDomain8]
SolverType = Stokes
DiscretizationScheme = Staggered
Grid.LowerLeft = 0 0.0
Grid.UpperRight = 6 1.5
Grid.Cells = 60 15
Problem.Name = stokes

[Mortar0]
Grid.LowerLeft = 0.0 0
Grid.UpperRight = 1.5 0.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar0

[Mortar1]
Grid.LowerLeft = 1.5 0
Grid.UpperRight = 3.0 0.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar1

[Mortar2]
Grid.LowerLeft = 3.0 0
Grid.UpperRight = 4.5 0.0
Grid.Cells = 10
ProjectionIntegrationOrder = 1
Problem.Name = mortar2

[Mortar3]
Grid.LowerLeft = 4.5 0
Grid.UpperRight = 6.0 0.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar3

[Mortar4]
Grid.LowerLeft = 1.5 0
Grid.UpperRight = 1.5 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar4

[Mortar5]
Grid.LowerLeft = 1.5 -1.5
Grid.UpperRight = 1.5 -3.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar5

[Mortar6]
Grid.LowerLeft = 3.0 0.0
Grid.UpperRight = 3.0 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar6

[Mortar7]
Grid.LowerLeft = 3.0 -1.5
Grid.UpperRight = 3.0 -3.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar7

[Mortar8]
Grid.LowerLeft = 4.5 0.0
Grid.UpperRight = 4.5 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar8

[Mortar9]
Grid.LowerLeft = 4.5 -1.5
Grid.UpperRight = 4.5 -3.0
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar9

[Mortar10]
Grid.LowerLeft = 0.0 -1.5
Grid.UpperRight = 1.5 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar10

[Mortar11]
Grid.LowerLeft = 1.5 -1.5
Grid.UpperRight = 3.0 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar11

[Mortar12]
Grid.LowerLeft = 3.0 -1.5
Grid.UpperRight = 4.5 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar12

[Mortar13]
Grid.LowerLeft = 4.5 -1.5
Grid.UpperRight = 6.0 -1.5
Grid.Cells = 15
ProjectionIntegrationOrder = 1
Problem.Name = mortar13

[Mortar]
VariableType = Pressure

[InterfaceSolver]
Verbosity = 2
ResidualReduction = 1e-8
MaxIterations = 1000
LinearSolverType = GMRes

[Vtk]
OutputName = mortarcoupling
AddVelocity = 1

[0.Component]
LiquidDensity = 1
LiquidKinematicViscosity = 1
