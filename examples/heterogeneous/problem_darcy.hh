// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem.
 */
#ifndef DUMUX_MORTAR_DARCY_SUBPROBLEM_HH
#define DUMUX_MORTAR_DARCY_SUBPROBLEM_HH

#include <limits>
#include <unordered_map>

#include <dune/alugrid/grid.hh>

#include <dumux/common/id.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <examples/common/mortarvariabletype.hh>
#include <examples/common/traceoperatorhelper.hh>
#include <examples/common/mpfa/properties.hh>

#include "spatialparams_darcy.hh"

namespace Dumux {
template <class TypeTag>
class DarcySubProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct DarcyOneP { using InheritsFrom = std::tuple<OneP>; };
struct DarcyOnePTpfa { using InheritsFrom = std::tuple<DarcyOneP, CCTpfaModel>; };
struct DarcyOnePMpfa { using InheritsFrom = std::tuple<DarcyOneP, MortarMpfa>; };
struct DarcyOnePBox { using InheritsFrom = std::tuple<DarcyOneP, BoxModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyOneP> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::Constant<0, Scalar> > ;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>;
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyOneP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePDarcySpatialParams<GridGeometry, Scalar>;
};

// Some optimization flags
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::DarcyOneP> { static constexpr bool value = false; };

} // end namespace Properties

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using IndexType = typename IndexTraits<GridView>::GridIndex;
    using CoupledScvfMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<IndexType>;
    using BoundaryTypes = Dumux::BoundaryTypes<1>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isBox = GridGeometry::discMethod == DiscretizationMethod::box;

public:
    //! Export spatial parameters type
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //! The constructor
    DarcySubProblem(std::shared_ptr<const GridGeometry> fvGridGeometry,
                    std::shared_ptr<SpatialParams> spatialParams,
                    const std::string& paramGroup,
                    MortarVariableType mv)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , mortarVariableType_(mv)
    , useHomogeneousSetup_(false)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        if (mv != MortarVariableType::solution && mv != MortarVariableType::derivative)
            DUNE_THROW(Dune::NotImplemented, "Unsupported mortar variable type");

        perms_.resize(fvGridGeometry->gridView().size(0));
        for (const auto& element : elements(fvGridGeometry->gridView()))
            perms_[fvGridGeometry->elementMapper().index(element)] =
                this->spatialParams().permeability(element);
    }

    //! Return the problem name.
    const std::string& name() const
    { return problemName_; }

    //! Returns the temperature within the domain in [K].
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C

    //! Specifies which kind of boundary condition should be used (overload for box)
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        if ( isOnMortarInterface_(element, scv).first )
        {
            BoundaryTypes values;
            if (mortarVariableType_ == MortarVariableType::solution)
                values.setAllDirichlet();
            else
                values.setAllNeumann();
            return values;
        }
        else
            return boundaryTypesAtPos(scv.dofPosition());
    }

    //! Specifies which kind of boundary condition should be used (overload for tpfa)
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        if ( isOnMortarInterface_(element, scvf).first )
        {
            BoundaryTypes values;
            if (mortarVariableType_ == MortarVariableType::solution)
                values.setAllDirichlet();
            else
                values.setAllNeumann();
            return values;
        }
        else
            return boundaryTypesAtPos(scvf.ipGlobal());
    }

    //! Specifies which kind of boundary condition should be used.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (globalPos[1] < this->gridGeometry().bBoxMin()[1] + 1e-6)
            values.setAllDirichlet();
        return values;
    }

    //! Evaluate the source term for an scv
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector source(0.0);
        return source;
    }

    //! Evaluates the Dirichlet boundary condition (overload for box)
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        const auto mortarIdPair = isOnMortarInterface_(element, scv);
        if ( mortarIdPair.first )
            return PrimaryVariables( mortarProjections_.at(mortarIdPair.second)[scv.dofIndex()] );
        return dirichletAtPos(scv.dofPosition());
    }

    //! Evaluates the Dirichlet boundary condition (overload for tpfa)
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto mortarIdPair = isOnMortarInterface_(element, scvf);
        if ( mortarIdPair.first )
            return PrimaryVariables( mortarProjections_.at(mortarIdPair.second)[scvf.insideScvIdx()] );
        return dirichletAtPos(scvf.ipGlobal());
    }

    //! Evaluates the Dirichlet boundary conditions
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (!useHomogeneousSetup_)
            return initialAtPos(globalPos);
        return PrimaryVariables(0.0);
    }

    //! Evaluates the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    { return neumann(element, fvGeometry, elemVolVars, scvf); }

    //! Evaluates the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto mortarIdPair = isOnMortarInterface_(element, scvf);
        if ( mortarIdPair.first )
        {
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            auto flux = mortarProjections_.at(mortarIdPair.second)[insideScv.elementIndex()];
            flux *= elemVolVars[insideScv].density();
            return NumEqVector(flux);
        }
        return NumEqVector(0.0);
    }

    //! Evaluates the initial condition at a given position
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.); }

    //! set the projected mortar solution
    void setMortarProjection(const Id& id, SolutionVector p)
    {
        mortarProjections_[id.get()] = p;
    }

    //! Set whether or not the homogeneous system is solved
    void setUseHomogeneousSetup(bool value)
    {
        useHomogeneousSetup_ = value;
    }

    //! add fields to the output module
    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        outputModule.addField(perms_, "permeability");
    }

    //! register an adjacent mortar segment
    template<class MortarSegment, class TraceOperator>
    void registerMortarSegment(const MortarSegment& mortarSegment, const TraceOperator& traceOperator)
    {
        coupledScvfMaps_[mortarSegment.id().get()] = &traceOperator.coupledScvfMap();
    }

    Scalar exactPressure(const GlobalPosition& globalPos) const
    { DUNE_THROW(Dune::InvalidStateException, "No exact solution available for this test"); }

    GlobalPosition exactVelocity(const GlobalPosition&) const
    { DUNE_THROW(Dune::NotImplemented, "Exact velocity"); }

    void setInterfacePermeabilityField(const Id& id, SolutionVector perms)
    {}

    const SolutionVector& permeability() const
    { return perms_; }

private:
    //! Returns true (plus the interface id) if scvf is on interface
    std::pair<bool, std::size_t> isOnMortarInterface_(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        for (const auto& pair : coupledScvfMaps_)
        {
            auto it = pair.second->find(eIdx);
            if (it != pair.second->end())
            {
                const auto& scvfList = it->second;
                if ( std::find(scvfList.begin(),
                               scvfList.end(),
                               scvf.index()) != scvfList.end() )
                    return std::make_pair(true, pair.first);
            }
        }

        return std::make_pair(false, std::numeric_limits<std::size_t>::max());
    }

    //! Returns true (plus the interface id) if scv is on interface
    std::pair<bool, std::size_t> isOnMortarInterface_(const Element& element, const SubControlVolume& scv) const
    { DUNE_THROW(Dune::NotImplemented, "Support for Box"); }

    std::string problemName_;
    MortarVariableType mortarVariableType_;
    std::unordered_map<std::size_t, const CoupledScvfMap*> coupledScvfMaps_;
    std::unordered_map<std::size_t, SolutionVector> mortarProjections_;
    bool useHomogeneousSetup_;
    SolutionVector perms_;
};

} // end namespace Dumux

#endif //DUMUX_DARCY_SUBPROBLEM_HH
