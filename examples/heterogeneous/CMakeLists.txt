dune_symlink_to_source_files(
    FILES
        "params.input"
        "spe_perm.dat"
        "prepare_mesh_and_perm_map.py"
        "reproduce_results.sh"
	"state_with_perm.pvsm"
)

dune_add_test(NAME heterogeneous_tpfa
              SOURCES main.cc
              CMAKE_GUARD "( HAVE_UMFPACK )"
              CMD_ARGS params.imput)

dune_add_test(NAME heterogeneous_mpfa
              SOURCES main.cc
              COMPILE_DEFINITIONS DARCYTYPETAG=DarcyOnePMpfa
              CMAKE_GUARD "( HAVE_UMFPACK )"
              CMD_ARGS params.imput)

set(CMAKE_BUILD_TYPE Release)
