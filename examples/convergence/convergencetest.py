import sys
import subprocess
from math import log
from numpy import loadtxt


def compile(exe: str) -> None:
    print(f"Compiling {exe}")
    subprocess.run(["make", exe], check=True)


def simulate(exe: str, refinements: int, runtime_args: list = []) -> dict:
    print(f"Simulating {refinements} refinements")
    errors: dict = {name: [] for name in ["p_d", "p_s", "v_d", "v_s", "lambda"]}
    for i in range(refinements + 1):
        print("\n" + "-"*50)
        print(f"Running refinement {i}")
        error_basefilename = f"errors_{i}"
        subprocess.run([
            f"./{exe}",
            "params.input",
            "-SubDomain0.Grid.Refinement", str(i),
            "-SubDomain1.Grid.Refinement", str(i),
            "-Mortar0.Grid.Refinement", str(i),
            "-IO.ErrorNormFileBaseName", error_basefilename
        ] + runtime_args, check=True)
        cur_darcy_errors = loadtxt(f"{error_basefilename}_0.csv", delimiter=",")
        errors["p_d"].append(cur_darcy_errors[0])
        errors["v_d"].append(cur_darcy_errors[1])
        cur_stokes_errors = loadtxt(f"{error_basefilename}_1.csv", delimiter=",")
        errors["p_s"].append(cur_stokes_errors[0])
        errors["v_s"].append(cur_stokes_errors[1])
        cur_mortar_errors = loadtxt(f"{error_basefilename}_mortar.csv", delimiter=",")
        errors["lambda"].append(cur_mortar_errors)
    return errors


def write_error_table(errors: dict, tex_basefilename: str) -> None:
    num_refinements = len(errors["p_d"])
    assert all(len(vals) == num_refinements for vals in errors.values())

    def _write_table(err_entries: list,
                     subscripts: list,
                     caption_suffix: str,
                     label: str,
                     tex_filename: str) -> None:
        with open(tex_filename, 'w') as tex_file:
            tex_file.write(r"\begin{table}[h]" + "\n")
            tex_file.write(r"    {\footnotesize" + "\n")
            tex_file.write(r"    \caption{\textbf{Convergence test}: " + caption_suffix + "}\n")
            tex_file.write(r"    \label{tab:" + label + "}\n")
            tex_file.write(r"    \begin{center}" + "\n")
            tex_file.write(r"    \begin{tabular}{l " + "|l l"*len(err_entries) + "}\n")
            tex_file.write(r"        \toprule" + "\n")
            tex_file.write(r"        $i$ & " + " & ".join("$e_{" + sub + "}$ & $r_{" + sub + "}$" for sub in subscripts) + r" \\" + "\n")
            tex_file.write(r"        \midrule" + "\n")

            for i in range(num_refinements):
                tex_file.write(f"\t{i}")

                for entry in err_entries:
                    tex_file.write(" & {:.2e}".format(errors[entry][i]) + " & ")
                    rate = (log(errors[entry][i]) - log(errors[entry][i-1]))/log(2) if i > 0 else None
                    tex_file.write("{:.2e}".format(rate) if rate else "")
                tex_file.write(r" \\" + "\n")

            tex_file.write(r"        \bottomrule" + "\n")
            tex_file.write(r"    \end{tabular}" + "\n")
            tex_file.write(r"    \end{center}" + "\n")
            tex_file.write(r"    }" + "\n")
            tex_file.write(r"\end{table}" + "\n")


    _write_table(
        err_entries=["p_d", "v_d", "p_s", "v_s", "lambda"],
        subscripts=["p, D", "u, D", "p, S", "u, S", r"\lambda"],
        caption_suffix="errors and convergence rates.",
        label="convtest",
        tex_filename=f"{tex_basefilename}.tex"
    )


if len(sys.argv) < 3:
    sys.stderr.write("Expected two arguments: the executable name & number of refinements")
    sys.exit(1)

compile(sys.argv[1])
runtime_args = sys.argv[3:] if len(sys.argv) > 3 else []
errors = simulate(sys.argv[1], int(sys.argv[2]), runtime_args)
write_error_table(errors, "convergence_table")
