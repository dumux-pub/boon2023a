#!/bin/bash

function check() {
    if ! $1; then
        echo "Command failed"
        exit 1;
    fi
}

NUM_REFINEMENTS=5
echo "Running convergence test"
check "python3 convergencetest.py convergence_tpfa ${NUM_REFINEMENTS}"
echo "Convergence rates written to convergence_table.tex"

echo "Rendering pressure image"
pvpython render_state.py --state-file pressure.pvsm -o convtest_p.png
echo "Saved in convtest_p.png"

echo "Rendering image for x-velocity"
pvpython render_state.py --state-file velocity_x.pvsm -o convtest_vx.png
echo "Saved in convtest_vx.png"

echo "Rendering image for y-velocity"
pvpython render_state.py --state-file velocity_y.pvsm -o convtest_vy.png
echo "Saved in convtest_vy.png"

echo "Moving results to folder 'convtest_results'"
rm -rf convtest_results
mkdir convtest_results
mv *.vtu *.png *.pvd *.vtp *.tex convtest_results

echo "Re-running convergence test with lower-order integration (super-convergence)"
ARGS=$(cat <<EOF
    -Error.MortarL2NormIntegrationOrder 0
    -Error.DarcyPressureL2IntegrationOrder 0
    -Error.DarcyVelocityL2IntegrationOrder 0
    -Error.StokesPressureL2IntegrationOrder 0
    -Error.StokesVelocityL2IntegrationOrder 0
    -Error.StokesVelocityH1IntegrationOrder 0
EOF
)
check "python3 convergencetest.py convergence_tpfa ${NUM_REFINEMENTS} ${ARGS//$'\n'/ }"

echo "Moving results to folder 'convtest_results_super_convergence'"
rm -rf convtest_results_super_convergence
mkdir convtest_results_super_convergence
mv *.vtu *.pvd *.vtp *.tex convtest_results_super_convergence
