// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Stokes test problem for the staggered grid (Navier-)Stokes model.
 */
#ifndef DUMUX_STOKES_SUBPROBLEM_HH
#define DUMUX_STOKES_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/common/id.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/geometry/makegeometry.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <examples/common/mortarvariabletype.hh>
#include <examples/common/traceoperatorhelper.hh>
#include "common.hh"

namespace Dumux {
template <class TypeTag>
class StokesSubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct StokesOneP { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::Constant<0, Scalar> > ;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesOneP>
{
    using HostGrid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >;
    using type = Dune::SubGrid<2, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesOneP> { using type = Dumux::StokesSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \ingroup BoundaryTests
 * \brief Test problem for the one-phase (Navier-) Stokes problem.
 *
 * Horizontal flow from left to right with a parabolic velocity profile.
 */
template <class TypeTag>
class StokesSubProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<ModelTraits::numEq()>;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using MortarSolutionVector = Dune::BlockVector< Dune::FieldVector<Scalar, 1> >;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using IndexType = typename IndexTraits<GridView>::GridIndex;
    using CoupledScvfMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<IndexType>;

public:
    StokesSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     const std::string& paramGroup,
                     MortarVariableType mv)
    : ParentType(fvGridGeometry, paramGroup)
    , eps_(1e-6)
    , mortarVariableType_(mv)
    , useHomogeneousSetup_(false)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        exactPressure_.resize(fvGridGeometry->gridView().size(0), 0.0);
        exactVelocity_.resize(fvGridGeometry->gridView().size(0), GlobalPosition(0.0));
        for (const auto& element : elements(fvGridGeometry->gridView()))
        {
            const auto eIdx = fvGridGeometry->elementMapper().index(element);
            const auto center = element.geometry().center();
            exactPressure_[eIdx] = exactPressure(center);
            exactVelocity_[eIdx] = exactVelocity(center);
        }
    }

    //! Return the problem name.
    const std::string& name() const
    { return problemName_; }

    //! Returns the temperature within the domain in [K].
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector source(0.0);

        if (!useHomogeneousSetup_)
        {
            using std::sin;
            const auto& quad = Dune::QuadratureRules<Scalar, GridView::dimension>::rule(element.geometry().type(), 2);
            for (auto&& qp : quad)
            {
                const auto pos = element.geometry().global(qp.position());
                auto integrationElement = element.geometry().integrationElement(qp.position());
                source[Indices::conti0EqIdx] += massSourceAtPos(pos)*qp.weight()*integrationElement;
            }
            source /= scv.volume();
        }

        return source;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace &scvf) const
    {
        NumEqVector source(0.0);

        if (!useHomogeneousSetup_)
        {
            using std::cos;
            using std::sin;

            std::vector<Dune::FieldVector<Scalar, 3>> corners(4);
            corners[0] = {scvf.corner(0)[0], scvf.corner(0)[1], 0};
            corners[1] = {scvf.corner(1)[0], scvf.corner(1)[1], 0};
            corners[2] = corners[0];
            corners[3] = corners[1];
            corners[2][scvf.directionIndex()] = element.geometry().center()[scvf.directionIndex()];
            corners[3][scvf.directionIndex()] = element.geometry().center()[scvf.directionIndex()];

            const auto geometry = makeDuneQuadrilaterial(corners);
            const auto& quad = Dune::QuadratureRules<Scalar, GridView::dimension>::rule(geometry.type(), 2);

            for (auto&& qp : quad)
            {
                const auto globalPos = geometry.global(qp.position());
                auto integrationElement = geometry.integrationElement(qp.position());
                const auto sourceAtPos = momentumSourceAtPos(GlobalPosition({globalPos[0], globalPos[1]}));
                source[Indices::momentumXBalanceIdx] += sourceAtPos[0]*qp.weight()*integrationElement;
                source[Indices::momentumYBalanceIdx] += sourceAtPos[1]*qp.weight()*integrationElement;
             }

            source /= element.geometry().volume()*0.5;
        }

        return source;
    }

    Scalar massSourceAtPos(const GlobalPosition& globalPos) const
    {
        const auto x = globalPos[0];
        using namespace StokesDarcyConvergenceTest;
        return -G()*omega()*omega()*sin(omega()*x);
    }

    GlobalPosition momentumSourceAtPos(const GlobalPosition& globalPos) const
    {
        using std::sin;
        using std::cos;
        const auto x = globalPos[0];
        const auto y = globalPos[1];
        GlobalPosition source(0.0);

        using std::pow;
        using namespace StokesDarcyConvergenceTest;
        source[0] = 2.0*mu()*(2.0 - x) - omega()*cos(omega()*x)/(2.0*k()) + 2.0*mu()*G()*pow(omega(), 3)*cos(omega()*x);
        source[1] = mu()*(2.0*y - 1.5 - xi() + (omega()*omega())*sin(omega()*x)) - M_PI*sin(M_PI*y);
        // source[0] = -0.5*k()*omega()*cos(omega()*x) - 2.0*mu()*(2.*x - 4.);
        // source[1] = 2.0*mu()*omega()*omega()*sin(omega()*x) - M_PI*sin(M_PI*y);
        return source;
    }

    Scalar exactPressure(const GlobalPosition& globalPos) const
    {
        using std::sin;
        using std::cos;

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        using namespace StokesDarcyConvergenceTest;
        return -1.0*(sin(omega()*x) + chi())/(2.0*k())
               + 2.0*mu()*(0.5 - xi())
               + cos(M_PI*y);
    }

    GlobalPosition exactVelocity(const GlobalPosition& globalPos) const
    {
        using std::sin;
        using std::cos;

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        using namespace StokesDarcyConvergenceTest;
        GlobalPosition velocity(0.0);
        velocity[0] = (2.0 - x)*(1.5 - y)*(y - xi()) + G()*omega()*cos(omega()*x);
        velocity[1] = -1.0*y*y*y/3.0 + y*y/2.0*(xi() + 1.5) - 1.5*xi()*y - 0.5 + sin(omega()*x);
        return velocity;
    }

    Scalar exactVelocityGradient(const GlobalPosition& globalPos, int velocityDir, int gradientDir) const
    {
        using namespace StokesDarcyConvergenceTest;
        using std::sin;
        using std::cos;

        const auto x = globalPos[0];
        const auto y = globalPos[1];
        if (velocityDir == 0)
        {
            if (gradientDir == 0)
                return -1.0*(1.5 - y)*(y - xi()) - G()*omega()*omega()*sin(omega()*x);
            else if (gradientDir == 1)
                return (2.0 - x)*(-1.0)*(y - xi()) + (2.0 - x)*(1.5 - y);
        }
        else if (velocityDir == 1)
        {
            if (gradientDir == 0)
                return omega()*cos(omega()*x);
            else if (gradientDir == 1)
                return -y*y + y*(xi() + 1.5) - 1.5*xi();
        }
        DUNE_THROW(Dune::InvalidStateException, "Unexpected velocity/gradient directions");
    }

    //! Specifies which kind of boundary condition should be used.
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        if (isOnMortarInterface_(element, scvf).first)
        {
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setBeaversJoseph(Indices::momentumXBalanceIdx);
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    //! Evaluates the velocity boundary conditions for a Dirichlet sub-control volume face.
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        // TODO get values from mortar space for flux mortars?
        if (!useHomogeneousSetup_)
        {
            PrimaryVariables values(0.0);
            values[Indices::velocityXIdx] = exactVelocity(scvf.ipGlobal())[0];
            values[Indices::velocityYIdx] = exactVelocity(scvf.ipGlobal())[1];
            return values;
        }
        else
            return PrimaryVariables(0.0);
    }

    //! Evaluates the pressure boundary conditions for a sub-control volume
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        // TODO get values from mortar space for flux mortars?
        if (!useHomogeneousSetup_)
        {
            PrimaryVariables values(0.0);
            values[Indices::velocityXIdx] = exactVelocity(scv.dofPosition())[0];
            values[Indices::velocityYIdx] = exactVelocity(scv.dofPosition())[1];
            return values;
        }
        else
            return PrimaryVariables(0.0);
    }

    //! Evaluates the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto mortarIdPair = isOnMortarInterface_(element, scvf);
        if (mortarIdPair.first)
        {
            NumEqVector values(0.0);

            const Scalar velocity = elemFaceVars[scvf].velocitySelf();
            const Scalar density = elemVolVars[scvf.insideScvIdx()].density();
            values[Indices::conti0EqIdx] = velocity*density*scvf.directionSign();

            const auto p = mortarProjections_.at(mortarIdPair.second)[scvf.insideScvIdx()];
            const auto initialP = initialAtPos(scvf.ipGlobal())[Indices::pressureIdx];
            values[scvf.directionIndex()] = (p - initialP)*scvf.directionSign();

            return values;
        }

        return NumEqVector(0.0);
    }

   //! Evaluates the initial conditions for a control volume.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter
              for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    { return StokesDarcyConvergenceTest::k(); }

    /*!
     * \brief Returns the alpha value required as input parameter for the
              Beavers-Joseph-Saffman boundary condition.
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    { return StokesDarcyConvergenceTest::alpha(); }

    //! Set whether or not the homogeneous system is solved
    void setUseHomogeneousSetup(bool value)
    {
        useHomogeneousSetup_ = value;
    }

    //! set the projected mortar solution
    void setMortarProjection(const Id& id, MortarSolutionVector p)
    {
        mortarProjections_[id.get()] = p;
    }

    //! register an adjacent mortar segment
    template<class MortarSegment, class TraceOperator>
    void registerMortarSegment(const MortarSegment& mortarSegment, const TraceOperator& traceOperator)
    {
        coupledScvfMaps_[mortarSegment.id().get()] = &traceOperator.coupledScvfMap();
    }

    //! add fields to the output module
    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        outputModule.addField(exactPressure_, "exact_pressure");
        outputModule.addField(exactVelocity_, "exact_velocity");
        for (auto& p : mortarProjections_)
            outputModule.addField(p.second, "mortar_" + std::to_string(p.first), OutputModule::FieldType::element);
    }

    void setInterfacePermeabilityField(const Id& id, MortarSolutionVector perms)
    {}

    const MortarSolutionVector& permeability() const
    {
        static MortarSolutionVector dummy(this->gridGeometry().gridView().size(0));
        dummy = 0.0;
        return dummy;
    }

private:
    //! Returns true (plus the interface id) if scvf is on interface
    std::pair<bool, std::size_t> isOnMortarInterface_(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        for (const auto& pair : coupledScvfMaps_)
        {
            auto it = pair.second->find(eIdx);
            if (it != pair.second->end())
            {
                const auto& scvfList = it->second;
                if ( std::find(scvfList.begin(),
                               scvfList.end(),
                               scvf.index()) != scvfList.end() )
                    return std::make_pair(true, pair.first);
            }
        }

        return std::make_pair(false, std::numeric_limits<std::size_t>::max());
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    Scalar eps_;
    std::string problemName_;

    MortarVariableType mortarVariableType_;
    bool useHomogeneousSetup_;

    std::unordered_map<std::size_t, const CoupledScvfMap*> coupledScvfMaps_;
    std::unordered_map<std::size_t, MortarSolutionVector> mortarProjections_;

    std::vector<Scalar> exactPressure_;
    std::vector<GlobalPosition> exactVelocity_;
};
} // end namespace Dumux

#endif // DUMUX_STOKES_SUBPROBLEM_HH
