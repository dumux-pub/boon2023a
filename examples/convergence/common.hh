// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_SD_MORTAR_EXAMPLES_CONVERGENCE_HH
#define DUMUX_SD_MORTAR_EXAMPLES_CONVERGENCE_HH

#include <cmath>
#include <dumux/common/parameters.hh>

namespace Dumux::StokesDarcyConvergenceTest {

double mu() {
    static const double mu = getParam<double>("0.Component.LiquidKinematicViscosity");
    return mu;
}

double k() {
    static const double k = getParam<double>("AnalyticalSolution.Permeability", 1.0);
    return k;
}

double alpha() {
    static const double alpha = getParam<double>("AnalyticalSolution.Alpha", 0.5);
    return alpha;
}

double G() {
    using std::sqrt;
    static const double G = sqrt(mu()*k())/alpha();
    return G;
}

double omega() {
    static const double omega = getParam<double>("AnalyticalSolution.Omega", 6.0);
    return omega;
}

double xi() {
    static const auto a = 1.0 - G();
    static const auto b = 2.0*(1.0 + G());
    return a/b;
}

double chi() {
    static const double chi = (-30.0*xi() - 17.0)/48.0;
    return chi;
}

} // end namespace Dumux

#endif // DUMUX_STOKES_SUBPROBLEM_HH
