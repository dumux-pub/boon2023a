import argparse
from paraview.simple import LoadState, Render, SaveScreenshot

parser = argparse.ArgumentParser(description="Render an image from a paraview state")
parser.add_argument("-s", "--state-file", required=True, help="The paraview state to render")
parser.add_argument("-o", "--output-file", required=True, help="The filename of the output image")
args = vars(parser.parse_args())

LoadState(args["state_file"])
Render()
SaveScreenshot(
    filename=args["output_file"],
    ImageResolution=(1626, 1460),
    TransparentBackground=1
)
