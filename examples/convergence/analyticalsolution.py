from sympy import *

x, y, omega, chi, xi, mu, K, pi, G = symbols("x y omega chi xi mu k pi G")
init_printing(use_unicode=True)

p = -(sin(omega*x) + chi)/2.0*K + mu*(0.5 - xi) + cos(pi*y) - 2.0*mu*G*omega**2*sin(omega*x)
gradpx = diff(p, x)
gradpy = diff(p, y)

ux = (2 - x)*(1.5 - y)*(y - xi) + G*omega*cos(omega*x)
uy = -y**3/3 + (y**2)/2*(xi + 1.5) - 1.5*xi*y - 0.5 + sin(omega*x)

print("DIV U = ", diff(ux, x) + diff(uy, y))

symgradu_xx = diff(ux, x)
symgradu_yy = diff(uy, y)
symgradu_xy = diff(ux, y) + diff(uy, x)

div_symgrad_x = diff(symgradu_xx, x) + diff(symgradu_xy, y)
div_symgrad_y = diff(symgradu_xy, x) + diff(symgradu_yy, y)

fx = gradpx - 2.0*mu*div_symgrad_x
fy = gradpy - 2.0*mu*div_symgrad_y

print("Sources stokes")
print("fx = ", fx)
print("fy = ", fy)

p = -(chi/K)*(y + 0.5)**2/2.0 - sin(omega*x)*y/K
gradpx = diff(p, x)
gradpy = diff(p, y)
# print(-1.0*K*gradpx)
# print(-1.0*K*gradpy)

print("Source darcy")
f = diff(-1.0*K*gradpx, x) + diff(-1.0*K*gradpy, y)
print(f)
