// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Stokes test problem for the staggered grid (Navier-)Stokes model.
 */
#ifndef DUMUX_STOKES_SUBPROBLEM_HH
#define DUMUX_STOKES_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/common/id.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/geometry/makegeometry.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include <examples/common/mortarvariabletype.hh>
#include <examples/common/traceoperatorhelper.hh>
#include "common.hh"

namespace Dumux {
template <class TypeTag>
class StokesSubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct StokesOneP { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesOneP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::Constant<0, Scalar> > ;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesOneP>
{
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2>>;
    using type = Dune::SubGrid<2, HostGrid>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesOneP> { using type = Dumux::StokesSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesOneP> { static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \ingroup BoundaryTests
 * \brief Test problem for the one-phase (Navier-) Stokes problem.
 *
 * Horizontal flow from left to right with a parabolic velocity profile.
 */
template <class TypeTag>
class StokesSubProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<ModelTraits::numEq()>;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using MortarSolutionVector = Dune::BlockVector< Dune::FieldVector<Scalar, 1> >;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using IndexType = typename IndexTraits<GridView>::GridIndex;
    using CoupledScvfMap = typename InterfaceTraceOperatorHelper::ElementScvfIndexMap<IndexType>;

public:
    StokesSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     const std::string& paramGroup,
                     MortarVariableType mv)
    : ParentType(fvGridGeometry, paramGroup)
    , eps_(1e-6)
    , mortarVariableType_(mv)
    , useHomogeneousSetup_(false)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        dummyPermeability_.resize(this->gridGeometry().gridView().size(0));
        dummyPermeability_ = 0.0;
    }

    //! Return the problem name.
    const std::string& name() const
    { return problemName_; }

    //! Returns the temperature within the domain in [K].
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolume& scv) const
    {
        NumEqVector source(0.0);
        return source;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace &scvf) const
    {
        NumEqVector source(0.0);
        return source;
    }

    //! Specifies which kind of boundary condition should be used.
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        if (isOnMortarInterface_(element, scvf).first)
        {
            using std::abs;
            values.setCouplingNeumann(Indices::conti0EqIdx);
            const bool isHorizontalInterface = abs(scvf.unitOuterNormal()[0]) < 1e-6;
            if (isHorizontalInterface)
            {
                values.setCouplingNeumann(Indices::momentumYBalanceIdx);
                values.setBeaversJoseph(Indices::momentumXBalanceIdx);
            }
            else
            {
                values.setCouplingNeumann(Indices::momentumXBalanceIdx);
                values.setBeaversJoseph(Indices::momentumYBalanceIdx);
            }
        }
        else if (onLeftBoundary_(scvf.ipGlobal()) || onRightBoundary_(scvf.ipGlobal()))
            values.setDirichlet(Indices::pressureIdx);
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    //! Evaluates the pressure boundary conditions for a sub-control volume
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        // TODO get values from mortar space for flux mortars?
        if (!useHomogeneousSetup_)
            return initialAtPos(globalPos);
        return PrimaryVariables(0.0);
    }

    //! Evaluates the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto mortarIdPair = isOnMortarInterface_(element, scvf);
        if (mortarIdPair.first)
        {
            const Scalar velocity = elemFaceVars[scvf].velocitySelf();
            const Scalar density = elemVolVars[scvf.insideScvIdx()].density();
            values[Indices::conti0EqIdx] = velocity*density*scvf.directionSign();

            const auto p = mortarProjections_.at(mortarIdPair.second)[scvf.insideScvIdx()];
            const auto initialP = initialAtPos(scvf.ipGlobal())[Indices::pressureIdx];
            values[scvf.directionIndex()] = (p - initialP)*scvf.directionSign();
        }

        return values;
    }

   //! Evaluates the initial conditions for a control volume.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
	static const double refPressure = getParam<double>("Problem.ReferencePressure", 0.0);
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = refPressure;
        if (onLeftBoundary_(globalPos))
            values[Indices::pressureIdx] += pressureDifference_();
        return values;
    }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter
              for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    {
        static const auto permeability = ObstacleTest::makePermeabilityTensor();
        static const Scalar normalPermeability = vtmv(scvf.unitOuterNormal(), permeability, scvf.unitOuterNormal());
        return normalPermeability;
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the
              Beavers-Joseph-Saffman boundary condition.
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        static const Scalar alphaBJ = getParam<Scalar>("Darcy.SpatialParams.AlphaBJ");
        return alphaBJ;
    }

    //! Set whether or not the homogeneous system is solved
    void setUseHomogeneousSetup(bool value)
    {
        useHomogeneousSetup_ = value;
    }

    //! set the projected mortar solution
    void setMortarProjection(const Id& id, MortarSolutionVector p)
    {
        mortarProjections_[id.get()] = p;
    }

    //! register an adjacent mortar segment
    template<class MortarSegment, class TraceOperator>
    void registerMortarSegment(const MortarSegment& mortarSegment, const TraceOperator& traceOperator)
    {
        coupledScvfMaps_[mortarSegment.id().get()] = &traceOperator.coupledScvfMap();
    }

    //! add fields to the output module
    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule) const
    {
        outputModule.addVolumeVariable([] (const auto& volVars) {
            return volVars.pressure() - 1.0e5;
        }, "deltap");
    }

    Scalar exactPressure(const GlobalPosition& globalPos) const
    { DUNE_THROW(Dune::InvalidStateException, "No exact solution available for this test"); }

    GlobalPosition exactVelocity(const GlobalPosition& globalPos) const
    { DUNE_THROW(Dune::InvalidStateException, "No exact solution available for this test"); }

    double exactVelocityGradient(const GlobalPosition& globalPos, int vdir, int cdir) const
    { DUNE_THROW(Dune::InvalidStateException, "No exact velocity gradient available for this test"); }

    void setInterfacePermeabilityField(const Id& id, MortarSolutionVector perms)
    {}

    const MortarSolutionVector& permeability() const
    { return dummyPermeability_; }

private:
    Scalar pressureDifference_() const
    {
        static const Scalar deltaP = getParam<Scalar>("Problem.PressureDifference");
        return deltaP;
    }

    //! Returns true (plus the interface id) if scvf is on interface
    std::pair<bool, std::size_t> isOnMortarInterface_(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        for (const auto& pair : coupledScvfMaps_)
        {
            auto it = pair.second->find(eIdx);
            if (it != pair.second->end())
            {
                const auto& scvfList = it->second;
                if ( std::find(scvfList.begin(),
                               scvfList.end(),
                               scvf.index()) != scvfList.end() )
                    return std::make_pair(true, pair.first);
            }
        }

        return std::make_pair(false, std::numeric_limits<std::size_t>::max());
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    Scalar eps_;
    std::string problemName_;

    MortarVariableType mortarVariableType_;
    bool useHomogeneousSetup_;

    std::unordered_map<std::size_t, const CoupledScvfMap*> coupledScvfMaps_;
    std::unordered_map<std::size_t, MortarSolutionVector> mortarProjections_;
    MortarSolutionVector dummyPermeability_;
};

} // end namespace Dumux

#endif // DUMUX_STOKES_SUBPROBLEM_HH
