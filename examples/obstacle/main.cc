// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryCoupling
 * \brief TODO doc me
 */
#include <config.h>
#include <iostream>
#include <memory>
#include <type_traits>

#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/foamgrid/foamgrid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/lagrangedgbasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/integrate.hh>

#include <dumux/discretization/fem/fegridgeometry.hh>
#include <dumux/multidomain/glue.hh>

#include <examples/common/l2error.hh>

#include "problem_darcy.hh"
#include "problem_stokes.hh"

#include <examples/common/helpers.hh>
#include <examples/common/subdomainsolvers.hh>
#include <examples/common/mortarvariabletype.hh>
#include <examples/common/interface.hh>
#include <examples/common/interfacemapper.hh>
#include <examples/common/mortarsegment.hh>
#include <examples/common/mergeandsplitsolutions.hh>

#include <examples/common/projector.hh>
#include <examples/common/projectorfactory.hh>
#include <examples/common/preconditioner.hh>
#include <examples/common/interfaceoperator.hh>

#ifndef DARCYTYPETAG
#define DARCYTYPETAG DarcyOnePTpfa
#endif

// Define grid and basis for mortar domain
struct MortarTraits
{
    static constexpr int basisOrder = 0;

    using Scalar = double;
    using Grid = Dune::FoamGrid<1, 2>;
    using GridView = typename Grid::LeafGridView;
    using BlockType = Dune::FieldVector<Scalar, 1>;
    using SolutionVector = Dune::BlockVector<BlockType>;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, basisOrder>;
    using GridGeometry = Dumux::FEGridGeometry<FEBasis>;
    static constexpr Dune::VTK::DataMode VTKDataMode = Dune::VTK::nonconforming;
};

// translate mortar variable into variable name for output
std::string getMortarVariableName(Dumux::MortarVariableType mv)
{ return mv == Dumux::MortarVariableType::solution ? "p" : "flux"; }


/////////////////////////////////
// The iterative solve routine //
/////////////////////////////////
template< class SubDomainSolver, class MortarSegment, class InterfaceMapper >
void solveIteratively( std::vector<std::shared_ptr<SubDomainSolver>> subDomainSolvers,
                       std::vector<std::shared_ptr<MortarSegment>> mortarSegments,
                       std::shared_ptr<InterfaceMapper> interfaceMapper,
                       Dumux::MortarVariableType mv )
{
    Dune::Timer watch;

    using namespace Dumux;

    // make vtk writer instances for all mortar segments
    using MortarGridGeometry = typename MortarSegment::GridGeometry;
    using MortarGridView = typename MortarGridGeometry::GridView;
    using MortarVTKWriter = Dune::VTKWriter<MortarGridView>;
    using MortarVTKSequenceWriter = Dune::VTKSequenceWriter<MortarGridView>;

    //! create a solution vector & a vtkWriter for each interface
    std::cout << "\n --- Initializing VTK output --- " << std::endl;
    using namespace Dune::Functions;
    using MortarSolutionVector = typename MortarTraits::SolutionVector;
    using MortarBlockType = typename MortarSolutionVector::block_type;
    using MortarGridFunction = std::decay_t<decltype(makeDiscreteGlobalBasisFunction<MortarBlockType>(mortarSegments[0]->gridGeometry().feBasis(),
                                                                                                      MortarSolutionVector{}))>;
    const auto numMortarSegments = mortarSegments.size();
    std::vector<MortarSolutionVector> mortarSolutionVectors(numMortarSegments);
    std::vector<std::shared_ptr<MortarGridFunction>> mortarGridFunctions(numMortarSegments);
    std::vector<std::shared_ptr<MortarVTKWriter>> mortarWriters(numMortarSegments);

    using Interface = typename InterfaceMapper::Interface;
    for (auto segmentPtr : mortarSegments)
    {
        auto& x = mortarSolutionVectors[segmentPtr->id().get()];
        x.resize(segmentPtr->gridGeometry().numDofs());
        x = 1.0e5;

        // project initial solution (=0.0) to sub domains
        MortarSolutionVector p1, p2;
        const auto& posInterface = interfaceMapper->getPositiveInterface(*segmentPtr);
        const auto& negInterface = interfaceMapper->getNegativeInterface(*segmentPtr);

        const auto& posProjector = posInterface.getProjector(Interface::ProjectionDirection::toSubDomain);
        const auto& negProjector = negInterface.getProjector(Interface::ProjectionDirection::toSubDomain);

        posInterface.subDomain().setMortarProjection(segmentPtr->id(), posProjector.project(x));
        negInterface.subDomain().setMortarProjection(segmentPtr->id(), negProjector.project(x));

        // create writer and add interface solution to output
        auto mortarVariableGF = makeDiscreteGlobalBasisFunction<MortarBlockType>(segmentPtr->gridGeometry().feBasis(), x);
        mortarGridFunctions[segmentPtr->id().get()] = std::make_shared<MortarGridFunction>(mortarVariableGF);
        mortarWriters[segmentPtr->id().get()] = std::make_shared<MortarVTKWriter>(segmentPtr->gridGeometry().gridView(), MortarTraits::VTKDataMode);

        const auto info = Dune::VTK::FieldInfo(getMortarVariableName(mv), Dune::VTK::FieldInfo::Type::scalar, 1);
        if (MortarTraits::basisOrder == 0)
            mortarWriters[segmentPtr->id().get()]->addCellData(*mortarGridFunctions[segmentPtr->id().get()], info);
        else
            mortarWriters[segmentPtr->id().get()]->addVertexData(*mortarGridFunctions[segmentPtr->id().get()], info);
    }

    //! create sequence writer for pvd output & write out initial solution
    std::vector<MortarVTKSequenceWriter> mortarSequenceWriters;
    mortarSequenceWriters.reserve(numMortarSegments);
    for (std::size_t i = 0; i < numMortarSegments; ++i)
        mortarSequenceWriters.emplace_back(mortarWriters[i], getParamFromGroup<std::string>("Mortar" + std::to_string(i), "Problem.Name"));

    std::cout << "\n --- Writing Initial VTK output --- " << std::endl;
    for (auto solverPtr : subDomainSolvers)
        solverPtr->write();

    // create interface operator
    using Operator = MortarInterfaceOperator<MortarSegment, SubDomainSolver, InterfaceMapper, MortarSolutionVector>;
    Operator op(mortarSegments, subDomainSolvers, interfaceMapper, mv);

    // first compute the initial jump in mortar variable
    std::cout << "\n --- Computing mortar variable jump --- " << std::endl;
    for (auto solverPtr  : subDomainSolvers)
        solverPtr->setState(SubDomainSolver::State::inhomogeneous);

    auto mortarSolution = mergeMortarSolutions(mortarSegments, mortarSolutionVectors);
    auto deltaMortarVariable = mortarSolution;
    mortarSolution = 0.0;
    deltaMortarVariable = 0.0;
    op.apply(mortarSolution, deltaMortarVariable);
    mortarSolution = 1.0e5;

    // Solve the homogeneous problem
    std::cout << "\n --- Solving homogeneous problem --- " << std::endl;
    for (auto solverPtr  : subDomainSolvers)
        solverPtr->setState(SubDomainSolver::State::homogeneous);

    deltaMortarVariable *= -1.0;
    Dune::InverseOperatorResult result;
    Dumux::MortarPreconditioner<MortarSolutionVector> prec;

    const auto lsType = getParam<std::string>("InterfaceSolver.LinearSolverType");
    const double reduction = getParam<double>("InterfaceSolver.ResidualReduction");
    const std::size_t maxIt = getParam<double>("InterfaceSolver.MaxIterations");
    const int verbosity = getParam<int>("InterfaceSolver.Verbosity");
    if (lsType == "CG")
    {
        Dune::CGSolver<MortarSolutionVector> cgSolver(op, prec, reduction, maxIt, verbosity);
        cgSolver.apply(mortarSolution, deltaMortarVariable, result);
    }
    else if (lsType == "GMRes")
    {
        Dune::RestartedGMResSolver<MortarSolutionVector> gmresSolver(op, prec, reduction, maxIt, maxIt, verbosity);
        gmresSolver.apply(mortarSolution, deltaMortarVariable, result);
    }
    else
        DUNE_THROW(Dune::NotImplemented, "Support for linear solver with name " << lsType);

    if (!result.converged)
        DUNE_THROW(Dune::InvalidStateException, "iterative solver did not converge in " << maxIt << " iterations");

    // solve the sub-domains again to get the right output
    std::cout << "\n --- Computing final solution --- " << std::endl;
    for (auto solverPtr  : subDomainSolvers)
        solverPtr->setState(SubDomainSolver::State::inhomogeneous);
    op.apply(mortarSolution, deltaMortarVariable);

    // extract sub-solutions from global mortar solution
    extractMortarSolutions(mortarSegments, mortarSolution, mortarSolutionVectors);

    // print time necessary for solve
    std::cout << "\n#####################################################\n\n"
              << "Iterative scheme took " << watch.elapsed() << " seconds\n"
              << "\n#####################################################\n\n";

    // Write VTK files
    std::cout << "\n --- Writing VTK output --- " << std::endl;
    for (auto solverPtr : subDomainSolvers)
        solverPtr->write();
    for (auto writer : mortarSequenceWriters)
        writer.write(1.0);
}

///////////////////////////////////////////////////////////////////
// Main Program. Selects the solvers etc to be passed to solve() //
///////////////////////////////////////////////////////////////////
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // get the mortar variable type specified in the input file
    const auto mortarVariableType = getParam<std::string>("Mortar.VariableType");
    MortarVariableType mvType = mortarVariableType == "Pressure" ? MortarVariableType::solution
                                                                 : MortarVariableType::derivative;

    ///////////////////////////////////////////////
    // create mortar grid geometries & segments
    using MortarGrid = typename MortarTraits::Grid;
    using MortarGridGeometry = typename MortarTraits::GridGeometry;
    using MortarSegment = Dumux::MortarSegment<MortarGridGeometry>;

    std::size_t segmentIndex = 0;
    std::vector<GridManager<MortarGrid>> mortarGridManagers;
    std::vector<std::shared_ptr<MortarGridGeometry>> mortarGridGeometries;
    std::vector<std::shared_ptr<MortarSegment>> mortarSegments;
    while (hasParamInGroup("Mortar" + std::to_string(segmentIndex), "Grid.File")
           || hasParamInGroup("Mortar" + std::to_string(segmentIndex), "Grid.Cells"))
    {
        mortarGridManagers.emplace_back();
        mortarGridManagers.back().init("Mortar" + std::to_string(segmentIndex));

        const auto& gridView = mortarGridManagers.back().grid().leafGridView();
        auto feBasis = std::make_shared<typename MortarTraits::FEBasis>(gridView);
        mortarGridGeometries.emplace_back( std::make_shared<MortarGridGeometry>(feBasis) );
        mortarSegments.emplace_back( std::make_shared<MortarSegment>(mortarGridGeometries.back(), Id(segmentIndex)) );

        segmentIndex++;
    }

    //////////////////////////////////////////////////////////////////////
    // interfaces between subdomains and mortar segments (+ mapper)
    using Position = typename MortarGrid::template Codim<0>::Entity::Geometry::GlobalCoordinate;
    using SolutionVector = typename MortarTraits::SolutionVector;
    using SubDomainSolver = Dumux::SubDomainSolver<SolutionVector, Position>;
    using Interface = Dumux::Interface<MortarSegment, SubDomainSolver, SolutionVector>;
    std::vector<std::shared_ptr<Interface>> interfaces;

    ////////////////////////////////////////////////////////////
    // make the sub-domain solvers and determine interfaces
    // TODO: Support Box scheme (choosable at runtime)
    using TTDarcyTpfa = Properties::TTag::DARCYTYPETAG;
    using TTStokes = Properties::TTag::StokesOneP;

    const auto subdomainExists = [&] (std::size_t solverIdx) {
        return hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.File")
           || hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.Cells")
           || hasParamInGroup("SubDomain" + std::to_string(solverIdx), "Grid.Cells0");
    };

    // determine number of sub-domains
    std::size_t numSubDomains = 0;
    while (subdomainExists(numSubDomains))
        numSubDomains++;

    // container with all sub-domain solvers
    std::vector<std::shared_ptr<SubDomainSolver>> solvers(numSubDomains);

    // lambda to add interfaces and trace operator for a new subdomain solver
    auto addSolver = [&] (auto& solverPtr, std::size_t solverIdx)
    {
        solverPtr->init("SubDomain" + std::to_string(solverIdx), Id(solverIdx), mvType);
        solvers[solverIdx] = solverPtr;

        // check for interfaces with the mortar segments
        const auto& gridGeometry = solverPtr->gridGeometry();
        for (auto segment : mortarSegments)
        {
            const auto glue = makeGlue(gridGeometry, segment->gridGeometry());

            // intersections were found
            if (glue.size() != 0)
            {
                auto& first = *intersections(glue).begin();
                auto sdFVGeometry = localView(gridGeometry);
                sdFVGeometry.bindElement(first.domainEntity());
                interfaces.emplace_back(std::make_shared<Interface>(
                    segment,
                    solvers[solverIdx],
                    getIntersectingNormalVector(sdFVGeometry, first.geometry())
                ));

                // make projectors between sub-domain and mortar segment
                std::shared_ptr<MortarProjectorInterface<SolutionVector>> toSubDomain;
                std::shared_ptr<MortarProjectorInterface<SolutionVector>> toMortarSegment;

                ProjectorFactory<SolutionVector> factory(mvType);
                factory.makeProjectors(*solverPtr, *segment, glue, toSubDomain, toMortarSegment);

                interfaces.back()->setProjectorToSubDomain(toSubDomain);
                interfaces.back()->setProjectorToMortarSegment(toMortarSegment);

                // register an interface in the sub-domain
                solverPtr->registerMortarSegment(*segment, glue);
            }
        }
    };

    // create sub-domain solvers
    std::size_t solverIdx = 0;
    while (subdomainExists(solverIdx))
    {
        using SolutionVector = typename MortarTraits::SolutionVector;
        const auto solverType = getParamFromGroup<std::string>("SubDomain" + std::to_string(solverIdx), "SolverType");
        if (solverType == "Darcy")
        {
            std::cout << "Creating Darcy solver" << std::endl;
            auto solver = std::make_shared< DarcySolver<TTDarcyTpfa, SolutionVector> >();
            addSolver(solver, solverIdx);
            std::cout << "done" << std::endl;
        }
        else if (solverType == "Stokes")
        {
            std::cout << "Creating Stokes solver" << std::endl;
            auto solver = std::make_shared< StokesSolver<TTStokes, SolutionVector> >();
            addSolver(solver, solverIdx);
            std::cout << "done" << std::endl;
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invlid solver type - " << solverType << " - specified");

        solverIdx++;
    }

    std::cout << "Created " << solverIdx << " subdomains, "
                            << segmentIndex << " mortar Segments and "
                            << interfaces.size() << " interfaces" << std::endl;

    if (solverIdx != numSubDomains)
        DUNE_THROW(Dune::InvalidStateException, "Number of created solvers does not match number of subdomains");

    if (interfaces.size() != segmentIndex*2)
        DUNE_THROW(Dune::InvalidStateException, "Number of created interfaces does not match number of segments");

    ///////////////////////////////
    // create interface mapper
    using InterfaceMapper = Dumux::InterfaceMapper<MortarSegment, Interface>;
    auto interfaceMapper = std::make_shared<InterfaceMapper>();
    for (auto interface : interfaces)
        interfaceMapper->addInterface(interface->mortarSegmentPointer(), interface);

    ///////////////////////////////
    // solve the problem iteratively
    std::cout << "\n --- Spinning up iterative solver --- " << std::endl;
    solveIteratively(solvers, mortarSegments, interfaceMapper, mvType);

    // print parameter settings
    if (mpiHelper.rank() == 0)
        Parameters::print();

    return 0;
} // end main
catch (Dumux::ParameterException& e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException& e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception& e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
