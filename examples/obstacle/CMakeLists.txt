dune_symlink_to_source_files(
    FILES
        "params.input"
        "darcy_domain.geo"
        "reproduce_results.sh"
)

dune_add_test(NAME obstacle_tpfa
              SOURCES main.cc
              CMAKE_GUARD "( HAVE_UMFPACK )"
              CMD_ARGS params.imput)

dune_add_test(NAME obstacle_mpfa
              SOURCES main.cc
              COMPILE_DEFINITIONS DARCYTYPETAG=DarcyOnePMpfa
              CMAKE_GUARD "( HAVE_UMFPACK )"
              CMD_ARGS params.imput)

set(CMAKE_BUILD_TYPE Release)
