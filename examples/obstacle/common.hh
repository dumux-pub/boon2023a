// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef BOON_2020A_OBSTACLE_COMMON_HH
#define BOON_2020A_OBSTACLE_COMMON_HH

#include <cmath>
#include <dune/common/fmatrix.hh>

namespace Dumux::ObstacleTest {

auto makePermeabilityTensor()
{
    Dune::FieldMatrix<double, 2, 2> permeability;

    double theta = getParam<double>("Darcy.SpatialParams.PermeabilityAngle", 45.0)*M_PI/180;
    double k1_ = getParam<double>("Darcy.SpatialParams.K1");
    double k2_ = getParam<double>("Darcy.SpatialParams.K2");

    double cost = cos(theta);
    double sint = sin(theta);
    permeability[0][0] = cost*cost*k1_ + sint*sint*k2_;
    permeability[1][1] = sint*sint*k1_ + cost*cost*k2_;
    permeability[0][1] = permeability[1][0] = cost*sint*(k1_ - k2_);
    return permeability;
}

} // end namespace Dumux::ObstacleTest

#endif
