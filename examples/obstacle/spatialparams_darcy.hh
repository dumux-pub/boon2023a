// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
#ifndef DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH
#define DUMUX_MORTAR_DARCY_SPATIALPARAMS_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv1p.hh>
#include "common.hh"

namespace Dumux {

/*!
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p model.
 */
template<class GridGeometry, class Scalar>
class OnePDarcySpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar,
                             OnePDarcySpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar,
                                           OnePDarcySpatialParams<GridGeometry, Scalar>>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

public:
    //! Export permeability type
    using PermeabilityType = DimWorldMatrix;

    //! The constructor
    OnePDarcySpatialParams(std::shared_ptr<const GridGeometry> fvGridGeometry,
                           const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    {
        permeability_ = ObstacleTest::makePermeabilityTensor();
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    /*! \brief Defines the porosity in [-].
     * \param globalPos The global position
     * \returns the porosity
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 0.4; }

private:
    PermeabilityType permeability_;
};

} // end namespace Dumux

#endif
