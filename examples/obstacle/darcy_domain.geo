SetFactory("OpenCASCADE");

DefineConstant[ dx = 0.03 ];
DefineConstant[ dx_corners = dx/5.0 ];
DefineConstant[ mesh_only_mortar = 0 ];

Rectangle(1) = {-0.125, 0, 0, 0.25, 0.2, 0};
Characteristic Length{:} = dx;

Field[1] = Attractor;
Field[1].NodesList = {3, 4};

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = dx_corners;
Field[2].LcMax = dx;
Field[2].DistMin = dx;
Field[2].DistMax = dx*8;

Background Field = 2;
Mesh.CharacteristicLengthExtendFromBoundary = 0;

If (mesh_only_mortar > 0)
    Physical Curve(1) = {mesh_only_mortar};
EndIf
