#!/bin/bash

make obstacle_mpfa && \
    gmsh -format msh2 -setnumber dx 0.01 -2 -o darcy_domain.msh darcy_domain.geo && \
    gmsh -format msh2 -setnumber dx 0.01 -setnumber mesh_only_mortar 4 -2 -o mortar_0.msh darcy_domain.geo && \
    gmsh -format msh2 -setnumber dx 0.01 -setnumber mesh_only_mortar 3 -2 -o mortar_1.msh darcy_domain.geo && \
    gmsh -format msh2 -setnumber dx 0.01 -setnumber mesh_only_mortar 2 -2 -o mortar_2.msh darcy_domain.geo && \
    ./obstacle_mpfa
