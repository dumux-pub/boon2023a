#!/usr/bin/env python3

# 
# This installs the module boon2023 and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |         dumux         |  origin/releases/3.4  |  f78393cff93e246d736819d9f6efc261ef733a1f  |  2021-11-30 18:47:58 +0000  |
# |      dune-alugrid     |  origin/releases/2.7  |  51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d  |  2021-04-22 15:10:17 +0200  |
# |      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
# |     dune-foamgrid     |     origin/master     |  d49187be4940227c945ced02f8457ccc9d47536a  |  2020-01-06 15:36:03 +0000  |
# |     dune-functions    |  origin/releases/2.7  |  534900aa256aeef9d025d5b8776ff1c0f0e144da  |  2020-10-29 19:47:28 +0000  |
# |     dune-geometry     |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |
# |       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
# |       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
# |  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
# |      dune-subgrid     |  origin/releases/2.7  |  45d1ee9f3f711e209695deee97912f4954f7f280  |  2020-05-28 13:21:59 +0000  |
# |     dune-typetree     |  origin/releases/2.7  |  50603353e821c1139f74871b575011fa44d13701  |  2020-08-11 11:56:10 +0000  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.split("/")[-1]
    if targetFolder.endswith(".git"):
        targetFolder = targetFolder[:-4]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/releases/3.4", "f78393cff93e246d736819d9f6efc261ef733a1f", )

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.7", "51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.7", "aa689abba532f40db8f5663fa379ea77211c1953", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "d49187be4940227c945ced02f8457ccc9d47536a", )

print("Installing dune-functions")
installModule("dune-functions", "https://gitlab.dune-project.org/staging/dune-functions.git", "origin/releases/2.7", "534900aa256aeef9d025d5b8776ff1c0f0e144da", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.7", "9d56be3e286bc761dd5d453332a8d793eff00cbe", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.7", "e8b460122a411cdd92f180dcca81ad8b2ac8d5ee", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.7", "761b28aa1deaa786ec55584ace99667545f1b493", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.7", "68f1bcf32d9068c258707d241624a08b771b6fde", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid.git", "origin/releases/2.7", "45d1ee9f3f711e209695deee97912f4954f7f280", )

print("Installing dune-typetree")
installModule("dune-typetree", "https://gitlab.dune-project.org/staging/dune-typetree.git", "origin/releases/2.7", "50603353e821c1139f74871b575011fa44d13701", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
