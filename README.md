# Boon2023a

This repository contains the code for the examples presented in

Boon, W., Gläser, D., Helmig, R., Weishaupt, K., Yotov, I. (2023).<br>
A mortar method for the coupled Stokes-Darcy problem using the MAC scheme for Stokes and mixed finite elements for Darcy (2023)<br>
Comput Geosci (2024). doi.org/10.1007/s10596-023-10267-6

DaRUS dataset: [![Identifier](https://img.shields.io/badge/doi-10.18419%2Fdarus--3598-d45815.svg)](https://doi.org/10.18419/darus-3598)


## Prerequisites

To compile the code, you need to have

- cmake
- c/c++/fortran compilers
- UMFPack (`libsuitesparse-dev`)

Note that we experienced problems compiling the code with `gcc-13`, but `gcc<12` should work fine. To select a specific compiler,
see the file `cmake.opts`.


## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|         dumux         |  origin/releases/3.4  |  f78393cff93e246d736819d9f6efc261ef733a1f  |  2021-11-30 18:47:58 +0000  |
|      dune-alugrid     |  origin/releases/2.7  |  51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d  |  2021-04-22 15:10:17 +0200  |
|      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
|     dune-foamgrid     |     origin/master     |  d49187be4940227c945ced02f8457ccc9d47536a  |  2020-01-06 15:36:03 +0000  |
|     dune-functions    |  origin/releases/2.7  |  534900aa256aeef9d025d5b8776ff1c0f0e144da  |  2020-10-29 19:47:28 +0000  |
|     dune-geometry     |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |
|       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
|       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
|  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
|      dune-subgrid     |  origin/releases/2.7  |  45d1ee9f3f711e209695deee97912f4954f7f280  |  2020-05-28 13:21:59 +0000  |
|     dune-typetree     |  origin/releases/2.7  |  50603353e821c1139f74871b575011fa44d13701  |  2020-08-11 11:56:10 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_boon2023.py`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/boon2023a.git boon2023
./boon2023/install_boon2023.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

